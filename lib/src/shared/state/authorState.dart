part of app.state;

class AddAuthor {
  // Registration response



  final String AuthorID;

  final TextEditingController NameController;
  final TextEditingController descriptionController;
  final TextEditingController QualificationController;
  final String photoPath;



  AddAuthor({
    this.AuthorID,
    this.NameController,
    this.descriptionController,
    this.QualificationController,
    this.photoPath,
  });

  factory AddAuthor.initial() {
    return new AddAuthor(
      NameController: new TextEditingController(),
      descriptionController: new TextEditingController(),
      QualificationController: new TextEditingController(),
      photoPath: "",
      AuthorID: "",
    );
  }

  AddAuthor copyWith({
    String AuthorID,
    String photoPath,
  }) {
    return new AddAuthor(
      NameController: this.NameController,
      descriptionController: this.descriptionController,
      QualificationController: this.QualificationController,
      AuthorID: AuthorID != null && AuthorID != "" ? AuthorID : this.AuthorID,
      photoPath: photoPath != null && photoPath != "" ? photoPath : this.photoPath,
    );
  }
}




class Author {
  // Registration response



  final String AuthorID;

  final String Name;
  final String description;
  final String Qualification;
  final String photoPath;



  Author({
    this.AuthorID,
    this.Name,
    this.description,
    this.Qualification,
    this.photoPath,
  });

  factory Author.initial() {
    return new Author(
      Name: "",
      description: "",
      Qualification: "",
      photoPath: "",
      AuthorID: "",
    );
  }

  Author copyWith({
     String Name,
     String description,
     String Qualification,
    String AuthorID,
    String photoPath,
  }) {
    return new Author(
      Name: Name != null && Name != "" ? Name : this.Name,
      description: description != null && description != "" ? description : this.description,
      Qualification: Qualification != null && Qualification != "" ? Qualification : this.Qualification,
      AuthorID: AuthorID != null && AuthorID != "" ? AuthorID : this.AuthorID,
      photoPath: photoPath != null && photoPath != "" ? photoPath : this.photoPath,
    );
  }


  Author.fromMap(Map<String, dynamic> data)
      : this(AuthorID: data['AuthorID'], Name: data['Name'], description: data['description'], Qualification: data['Qualification'], photoPath: data['photoPath'] );

  Map<String, dynamic> toMap() => {
    'AuthorID': this.AuthorID,
    'Name': this.Name,
    'description': this.description,
    'Qualification': this.Qualification,
    'photoPath': this.photoPath,
  };


}





final AddAuthorreducer = combineReducers<AppState>([
  TypedReducer<AppState, addImagePath>(_addImagePath),

]);

class addImagePath {
  final String value;
  addImagePath(this.value);
}

AppState _addImagePath(
    AppState state, addImagePath action) {
  return state.copyWith(
      addAuthor: state.addAuthor.copyWith(photoPath: action.value));
}