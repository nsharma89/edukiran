part of app.widgets;


class RegisterPage extends StatefulWidget {
  RegisterPage({
    Key key,
  }) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  String _email, _password;
  bool _isObscured = true;
  Color _eyeButtonColor = Colors.grey;
  Future<String> _message = Future<String>.value('');

  String verificationId;
  String testSmsCode;

  Future<void> _testVerifyPhoneNumber(String Controller , String au) async {
    final PhoneVerificationCompleted verificationCompleted =
        (FirebaseUser user) {
      setState(() {
        _message =
            Future<String>.value('signInWithPhoneNumber auto succeeded: $user');
      });
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      setState(() {
        _message = Future<String>.value(
        'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
        });
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      Controller = testSmsCode;
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      this.verificationId = verificationId;
      Controller = testSmsCode;
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: au,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<String> _testSignInWithPhoneNumber(String Controller ,String smsCode) async {
    final FirebaseUser user = await _auth.signInWithPhoneNumber(
      verificationId: verificationId,
      smsCode: smsCode,
    );

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    Controller = '';
    return 'signInWithPhoneNumber succeeded: $user';
  }

  static Function(String s) Email(String invalidMessage) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  static Function(String s) PhoneNumberValidator(String invalidMessage) {
    String p = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  static Function(String s) Date(String invalidMessage) {
    String p = r'^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/([12]\d{3})$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  Padding buildTitle(String txt) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        txt,
        style: TextStyle(fontSize: 42.0),
      ),
    );
  }

  Padding buildTitleLine() {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0, left: 12.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          width: 38.0,
          height: 1.5,
          color: Colors.black,
        ),
      ),
    );
  }

  TextFormField buildEmailTextField() {
    return TextFormField(
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an email';
        }
      },
      decoration: InputDecoration(labelText: 'Email Address'),
    );
  }

  TextFormField PhoneTextField(TextEditingController controller) {
    return TextFormField(
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an email';
        }
      },
      decoration: InputDecoration(labelText: 'Mobile Number'),
      controller: controller,
      keyboardType: TextInputType.number,
    );
  }

  dob(AddUserModel au) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0,right: 16.0),
      child: TextFormField(
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(0.0),
            hintText: "DD/MM/YYYY",
            prefixIcon: Icon(Icons.date_range),
            labelText: "Birth Date",
            enabled: true,
          ),
//          validator: (value) {
//            if (value == "") {
//              return "Date is required";
//            } else {
//              Date("Date is not valid");
//            }
//          },
          controller: au.addUserState.DOBController,
          keyboardType: TextInputType.datetime,
          inputFormatters: [
            WhitelistingTextInputFormatter.digitsOnly,
          ],

        ),
    );
  }

  TextFormField StateTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an State';
        }
      },
      decoration: InputDecoration(labelText: 'State'),
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.sentences,
    );
  }


  Padding buildPasswordText() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Align(
        alignment: Alignment.centerRight,
        child: Text(
          'Forgot Password?',
          style: TextStyle(fontSize: 12.0, color: Colors.grey),
        ),
      ),
    );
  }

  Align buildLoginButton(BuildContext context, AddUserModel au , String path) {
    return Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: () {
            au.VerifyeViewModel.onPressedCallback();
            Navigator.pushNamed(context, path);
          },
          color: Colors.grey[900],
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Verify',
            style: Theme.of(context).primaryTextTheme.button,
          ),
        ),
      ),
    );
  }

  Align buildVerifyButton(BuildContext context, AddUserModel au, String path) {
    return Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: () {
            if (au.addUserState.PhoneNumController.text != null) {
              au.phoneAuthButtonViewModel.onPressedCallback();
              Navigator.pushNamed(context , path);
            }
          },
          color: Colors.grey[900],
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Send Otp',
            style: Theme.of(context).primaryTextTheme.button,
          ),
        ),
      ),
    );
  }





  Align buildOrText() {
    return Align(
      alignment: Alignment.center,
      child: Text(
        'or login with',
        style: TextStyle(fontSize: 12.0, color: Colors.grey),
      ),
    );
  }

  Container buildSocialMediaButtons(IconData icon, Color iconColor) {
    return Container(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, "/SignUp");
        },
        borderRadius: BorderRadius.circular(30.0),
        child: Icon(icon, color: iconColor),
      ),
      height: 46.0,
      width: 46.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.grey, width: 0.5)),
    );
  }

  Align buildSignUpText(BuildContext context, String path) {
    return Align(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, path);
        },
        borderRadius: BorderRadius.circular(30.0),
        child: RichText(
          text: TextSpan(
              text: 'You Don\'t have an account?',
              style: TextStyle(fontSize: 12.0, color: Colors.grey),
              children: <TextSpan>[
                TextSpan(
                    text: ' SIGN UP',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                        color: Colors.black)),
              ]),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        child:  new StoreConnector<AppState, AddUserModel>(
        converter: (store) {
      return AddUserModel(
        addUserState: store.state.addUserState,
        loadingState: store.state.loadingState,
        onOccupationStudent: (s) {
          store.dispatch(OccupationDropdownChange('Student'));
        },
        onOccupationTEacher: (s) {
          FocusScope.of(context).requestFocus(new FocusNode());
          store.dispatch(OccupationDropdownChange(s));
        },
        onGenderChange: (s) {
          store.dispatch(GenderDropdownChange(s));
        },
        onSave: () {
          if (_formKey.currentState.validate()) {
          store.dispatch(UserAdded());}
        },
      );
    },
    builder: (BuildContext context, AddUserModel au) {
    return new Scaffold(

    body: Form(
     key: _formKey,
    child: SingleChildScrollView (
            padding: const EdgeInsets.fromLTRB(
      22.0, 0.0, 22.0, 22.0),
      child: Column(

      children: <Widget>[
      SizedBox(height: kToolbarHeight),
      buildTitle('Sign Up'),
      buildTitleLine(),
      SizedBox(
      height: 20.0,
      ),
      CardSettings(
      labelAlign: TextAlign.left,
      labelPadding: 0.0,
      contentAlign: TextAlign.left,

      children: <Widget>[

      SizedBox(
      height: 10.0,
      ),
      Padding(
        padding: const EdgeInsets.only(left: 16.0,right: 16.0),
        child: TextFormField(
          decoration: InputDecoration(
              labelText: 'Username',
              prefixIcon: Icon(Icons.perm_identity),
               hintText: "Ram Sharma",
          ),
          controller: au.addUserState.UserNameController,
          keyboardType: TextInputType.text,
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter your name';
            }
          } ,
        ),
      ),

//    CardSettingsText(
//    controller:au.addUserState.UserNameController ,
//    label: "Username",
//    hintText: "Ram Sharma",
//    style:TextStyle(textBaseline: TextBaseline.alphabetic) ,
//
//    ),
        SizedBox(
          height: 10.0,
        ),

        Padding(
          padding: const EdgeInsets.only(left: 16.0,right: 16.0),
          child: TextFormField(
            decoration: InputDecoration(
                labelText: 'Email',
                prefixIcon: Icon(Icons.mail_outline)
            ),
            controller: au.addUserState.emailController,
            keyboardType: TextInputType.emailAddress,
            validator: Email("Enter the correct Email Address")
            ,
          ),
        ),

//      CardSettingsEmail(
//    label: "Email",
//    labelAlign: TextAlign.left,
//    controller: au.addUserState.emailController,
//    validator: Email("Invalid Email "),
//    ),


      SizedBox(
      height: 10.0,
      ),
      CardSettingsField(
      label: "Occupation",

      labelAlign: TextAlign.left,
      content: FlutterSearchPanel(

          padding: EdgeInsets.all(10.0),
        title: 'Occupation',
        data: ['Teacher', 'Student'],
        icon: new Icon(Icons.check_circle, color: Colors.white),
        color: Colors.teal,
        textStyle: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20.0, decorationStyle: TextDecorationStyle.dotted),
        onChanged: (value) {
          print(value);
          au.onOccupationTEacher(value);
        },

      ),

      ),
      SizedBox(
      height: 30.0,
      ),
      dob(au),

        SizedBox(
          height: 10.0,
        ),

//      Padding(
//        padding: const EdgeInsets.only(left: 16.0,right: 16.0),
//        child: TextFormField(
//          decoration: InputDecoration(
//            labelText: 'Gender',
//            prefixIcon: Icon(Icons.perm_identity),hintText: "Male",
//          ),
//          controller: au.addUserState.GenderController,
//          keyboardType: TextInputType.text,
//          validator: (value) {
//            if (value.isEmpty) {
//              return 'Please enter your name';
//            }
//          },
//        ),
//      ),

   CardSettingsField(
       label: "Gender",
       labelAlign: TextAlign.center,

       content: FlutterSearchPanel(
         padding: EdgeInsets.all(10.0),
         selected: 'Male',
         title: 'Gender',
         data: ['Male', 'Female'],
         icon: new Icon(Icons.check_circle, color: Colors.white),
         color: Colors.teal,
         textStyle: new TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20.0, decorationStyle: TextDecorationStyle.dotted),
         onChanged: (value) {
           print(value);
           au.onGenderChange(value);
         },
       ),

   ),


//    CardSettingsText(
//    controller: au.addUserState.GenderController,
//    label: "Gender",
//    hintText: "Male",
//
//    ),

        SizedBox(
          height: 10.0,
        ),

        Padding(
          padding: const EdgeInsets.only(left: 16.0,right: 16.0),
          child: TextFormField(
            decoration: InputDecoration(
              labelText: 'Address',
              prefixIcon: Icon(Icons.account_balance),hintText: "Flat no - 0 1",
            ),
            controller: au.addUserState.AddressController,
            keyboardType: TextInputType.text,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please enter your Address';
              }
            },
          ),
        ),





//    CardSettingsText(
//    controller: au.addUserState.AddressController,
//    label: "Address",
//    hintText: "Flat no - 0 1",
//
//    ),

        SizedBox(
          height: 10.0,
        ),

        Padding(
          padding: const EdgeInsets.only(left: 16.0,right: 16.0),
          child: TextFormField(
            decoration: InputDecoration(
                labelText: 'State',
                prefixIcon: Icon(Icons.account_balance),hintText: "New Delhi",
            ),
            controller: au.addUserState.StateController,
            keyboardType: TextInputType.text,
//          validator: (value) {
//            if (value.isEmpty) {
//              return 'Please enter your State';
//            }
//          },
          ),
        ),




//    CardSettingsText(
//    controller:au.addUserState.StateController ,
//    label: "State",
//    hintText: "New Delhi",
//
//    ),

        SizedBox(
          height: 10.0,
        ),

        Padding(
          padding: const EdgeInsets.only(left: 16.0,right: 16.0),
          child: TextFormField(
            decoration: InputDecoration(
                labelText: 'City',
                prefixIcon: Icon(Icons.account_balance),
              hintText: "Jaipur",
            ),
            controller: au.addUserState.CityController,
            keyboardType: TextInputType.text,
//          validator: (value) {
//            if (value.isEmpty) {
//              return 'Please enter your City';
//            }
//          },
          ),
        ),





//    CardSettingsText(
//    controller:au.addUserState.CityController ,
//    label: "City",
//    hintText: "Jaipur",
//    ),

      SizedBox(
      height: 30.0,
      ),

        Align(
          child: SizedBox(
            height: 50.0,
            width: 270.0,
            child: FlatButton(
              onPressed: au.onSave,
              color: Colors.teal,
              shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
              child: Text(
                'Submit',
              ),
            ),
          ),
        ),

        SizedBox(
          height: 30.0,
        ),
      ],
      ),
      SizedBox(
      height: 30.0,
      ),



      SizedBox(height: 70.0),

      ],
      ),
    ),
    ),
    );
    ;
    },
    ));
  }
}



