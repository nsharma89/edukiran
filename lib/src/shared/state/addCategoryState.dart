part of app.state;

class AddCategoryVedio {
  // Registration response
  final String CategoryID;

  final List<String> tags;

  final TextEditingController NameController;
  final TextEditingController descriptionController;
  final TextEditingController tagsController;

  final List<String> Subcategory;

  final String CategoryName;

  //SubCategory
  final TextEditingController SubCategoryName;

  AddCategoryVedio(
      {this.CategoryID,
      this.tags,
      this.NameController,
      this.descriptionController,
      this.tagsController,
      this.Subcategory,
      this.CategoryName,
      this.SubCategoryName});

  factory AddCategoryVedio.initial() {
    return new AddCategoryVedio(
      NameController: new TextEditingController(),
      descriptionController: new TextEditingController(),
      tagsController: new TextEditingController(),
      CategoryID: "",
      tags: [],
      Subcategory: [],
      CategoryName: "Select one",
      SubCategoryName: new TextEditingController(),
    );
  }

  AddCategoryVedio copyWith({
    String CategoryID,
    List<String> Subcategory,
    List<String> tags,
    String CategoryName,
    String SubCategoryName,
  }) {
    return new AddCategoryVedio(
      NameController: this.NameController,
      descriptionController: this.descriptionController,
      tagsController: this.tagsController,
      CategoryID:
          CategoryID != null && CategoryID != "" ? CategoryID : this.CategoryID,
      CategoryName: CategoryName != null && CategoryName != ""
          ? CategoryName
          : this.CategoryName,
      SubCategoryName: this.SubCategoryName,
      Subcategory: Subcategory != null && Subcategory != ""
          ? Subcategory
          : this.Subcategory,
      tags: tags != null && tags != "" ? tags : this.tags,
    );
  }
}

class assignSubCategory {
  final String value;
  assignSubCategory(this.value);
}

AppState _assignSubCategory(AppState state, assignSubCategory action) {
  return state.copyWith(
      addCategoryVedio:
          state.addCategoryVedio.copyWith(CategoryName: action.value));
}

final addCategoryreducer = combineReducers<AppState>([
  TypedReducer<AppState, assignSubCategory>(_assignSubCategory),
]);
