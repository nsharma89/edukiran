import 'package:googleapis_auth/auth.dart';
import 'package:http/http.dart'
    show BaseRequest, BaseClient , Response, StreamedResponse;

import 'package:http/io_client.dart';

class GoogleHttpClient extends IOClient {
  Map<String, String> _headers;


  GoogleHttpClient(this._headers) : super();

//  @override
//  Future<StreamedResponse> send(BaseRequest request) =>
//      super.send(request..headers.addAll(_headers));

  @override
  Future<Response> head(Object url, {Map<String, String> headers}) =>
      super.head(url, headers: headers..addAll(_headers));

  @override
  Future<StreamedResponse> send(BaseRequest request) {
    // TODO: implement send
    return super.send(request..headers.addAll(_headers)); // ignore: abstract_super_member_reference
  }

}

