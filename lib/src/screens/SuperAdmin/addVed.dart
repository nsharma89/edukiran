part of app.widgets;

class AddVedScreen extends StatefulWidget {
  AddVedScreen({
    Key key,
  }) : super(key: key);

  @override
  _AddVedScreenState createState() => _AddVedScreenState();
}

class _AddVedScreenState extends State<AddVedScreen> {
  String _filePath;
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static GlobalKey<ScaffoldState> ScaffoldStatekey = GlobalKey<ScaffoldState>();



  void getFilePath(AddUserModel au) async {
    try {
      File filePath = await ImagePicker.pickVideo(source: ImageSource.gallery);
      if (filePath == '') {
        return;
      }

      print("File path: " + filePath.path);
      setState((){this._filePath = filePath.path;
      au.anything();
      });
      print(au.addYoutubeVedio.filePath);
    } on PlatformException catch (e) {
      print("Error while picking the file: " + e.toString());
    }
  }

  Widget addtextfield(String LabelText, IconData IconS ,TextEditingController controller ,{Function validator , int Maxlines}){
    return Padding(
      padding: const EdgeInsets.only(left: 16.0,right: 16.0),
      child: TextFormField(
        decoration: InputDecoration(
            labelText: LabelText,
            prefixIcon: Icon(IconS),
          fillColor: Colors.deepPurple

        ),
        controller: controller,
        keyboardType: TextInputType.text,
        validator: validator,
        maxLines: Maxlines,

      ),
    );
  }



  @override
  Widget build(BuildContext context) {

    return new StoreConnector<AppState, AddUserModel>(
      converter: (store) {
        return AddUserModel(
            addUserState: store.state.addUserState,
            loadingState: store.state.loadingState,
            addYoutubeVedio: store.state.addYoutubeVedio,
            anything: (){
              store.dispatch(addvedioPath(_filePath));
              print("ok");
            },
            onSave: (){
              if(_filePath != null){
              if(_formKey.currentState.validate()) {
                store.dispatch(refreshScreen1());
                store.dispatch(AddingVedio());
              }}else{
                ScaffoldStatekey.currentState.showSnackBar(TutSnackbar.error(text: "Please select a Video file"));
              }
            },
          navigate: (s) {
            if(s == "addVedio"){
              store.dispatch(advScreen());
            }
            if(s == "usm"){
              store.dispatch(UserManagementScreen());
            }
            if(s == "aly"){
              store.dispatch(anlyScreen());
            }

            if(s == "CScree"){
              store.dispatch(CScreen());
            }
            if(s == "Author"){
              store.dispatch(AuthorScreen1());
            }
            if(s == "Course"){
              store.dispatch(CourseScreen1());
            }

            if(s == "set"){
              store.dispatch(SettingsScreen());
            }
            if(s == "prof"){
              store.dispatch(profileScreen1());
            }


          },


        );
      },
      builder: (BuildContext context, AddUserModel au) {
        return Scaffold(
          key: ScaffoldStatekey,
          body: CustomScrollView(
            primary: false,
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 150.0,
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0 ,right:8.0 ),
                    child: IconButton(
                      icon: Icon(Icons.perm_identity),
                      onPressed: () => au.navigate("prof"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0,right:8.0 ),
                    child: IconButton(
                      icon: Icon(Icons.settings),
                      onPressed: () => au.navigate("set"),
                    ),
                  ),

                ],
                backgroundColor: Colors.deepPurple[100],
                flexibleSpace: FlexibleSpaceBar(
                  title: Text('Add Video'),
                  background: Image.asset("asset/fg3.jpg" ,fit: BoxFit.fitWidth,height: 150.0,),
                  collapseMode: CollapseMode.parallax,

                ),
              ),

              SliverPadding(
                  padding: const EdgeInsets.only(left: 5.0,right: 5.0,bottom: 20.0,top: 20.0),
                  sliver: SliverList(
                      delegate: SliverChildBuilderDelegate((builder , index){
                        return Form(
                          key: _formKey,
                          child: CardSettings(

                            labelAlign: TextAlign.left,
                            labelPadding: 0.0,
                            contentAlign: TextAlign.left,
                            children: <Widget>[
                              SizedBox(
                                height: 30.0,
                              ),
                              InkWell(
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Container(
                                    child: Center(
                                        child: Column(
                                          children: <Widget>[
                                            Icon(Icons.cloud_upload,size: 90.0,color: Colors.deepPurple,),
                                            Text("Add Path  $_filePath",textAlign: TextAlign.center,),
                                          ],
                                        )),
                                    height: 150.0,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      border: Border.all(color: Colors.deepPurple,style: BorderStyle.solid,width: 1.0)
                                    ),
                                  ),
                                ),
                                onTap:() {
                                  getFilePath(au);
                                },
                                splashColor: Colors.deepPurple,
                              ),
                              SizedBox(
                                height: 30.0,
                              ),
                              addtextfield( "Title",  Icons.title , au.addYoutubeVedio.TitleController,
                                validator: (value) {
                                if (value.isEmpty) {
                                  return 'Please enter Title';
                                }
                              },
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 16.0,bottom: 16.0),
                                child: addtextfield( "Description",  Icons.dehaze , au.addYoutubeVedio.descriptionController,
                                  validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please enter Description';
                                  }
                                },
                                    Maxlines: 3
                                ),
                              ),
                              SizedBox(
                                height: 30.0,
                              ),
                              CardSettingsMultiselect(
                                options: ["English","Grammar","Algorithm",
                                "Spanish","France","AlienVoice"
                                ],
                                label: "Tags",
                                initialValues: ["English","Grammar","AlienVoice",
                                ],
                                onChanged: (v){},
                                onSaved: (v){
                                },

                              ),
                              SizedBox(
                                height: 20.0,
                              ),
                              Align(
                                child: SizedBox(
                                  height: 50.0,
                                  width: 270.0,
                                  child: FlatButton(
                                    onPressed: () => au.onSave(),
                                    color: Colors.deepPurple,
                                    shape:
                                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                                    child: Text(
                                      'Upload Vedio',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                height: 30.0,
                              ),

                            ],
                          ),
                        );
                      },
                        childCount: 1,
                      ))
              ),

            ],
          ),

        );


      },
    );
  }
}