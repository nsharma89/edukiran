part of app.widgets;

class UserManageScreen extends StatefulWidget {
  UserManageScreen({
    Key key,
  }) : super(key: key);

  @override
  _UserManageScreenState createState() => _UserManageScreenState();
}

class _UserManageScreenState extends State<UserManageScreen> {





  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, AddUserModel>(
      converter: (store) {
        return AddUserModel(
            addUserState: store.state.addUserState,
            loadingState: store.state.loadingState,
            onMobilecode: (s){
              store.dispatch(CodeDropdownChange(s));
            }
        );
      },
      builder: (BuildContext context, AddUserModel au) {
        return Scaffold(
          appBar: AppBar(
            title: Text("UserManagement Screen"),
          ),

        );
      },
    );;
  }
}