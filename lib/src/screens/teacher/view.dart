part of app.widgets;

//class TeacherHomeApp extends StatefulWidget {
//  @override
//  _TeacherHomeAppState createState() => _TeacherHomeAppState();
//}
//
//class _TeacherHomeAppState extends State<TeacherHomeApp> {
//  @override
//  Widget build(BuildContext context) {
//    var  u = store.state.firebaseUser.uid;
//
//    Firestore.instance.collection('Users').document(u).get().then((datasnapshot){
//      if (datasnapshot.exists) {
//        var User = store.state.user;
//        User.copyWith(
//            uiid: datasnapshot.data['id'],
//            UserName: datasnapshot.data['UserName'],
//            gender: datasnapshot.data['gender'],
//            email: datasnapshot.data['email'],
//            phone: datasnapshot.data['phone'],
//            address: datasnapshot.data['address'],
//            city: datasnapshot.data['city'],
//            DOB: datasnapshot.data['DOB'],
//            state: datasnapshot.data['state'],
//            age: datasnapshot.data['age'],
//            date_register: datasnapshot.data['date_register'],
//            imageUri: datasnapshot.data['imageUri'],
//            Occupation: datasnapshot.data['Occupation'],
//            // EduCareId: datasnapshot.data['EduCareId'],
//            EduCareId: "abc"
//        );
//
//        if(datasnapshot.data['Occupation'].contains("Teacher") ){
//          store.dispatch(new TeacherHomeAppScreen());
//        }else if(datasnapshot.data['Occupation'].contains("SuperAdmin") ){
//          store.dispatch(new SuperAdminScreen());
//        }else if(datasnapshot.data['Occupation'].contains("paidStudent") ){
//          store.dispatch(new paidStudentScreen());
//        }else if(datasnapshot.data['Occupation'].contains("normalStudent") ){
//          store.dispatch(new normalStudentScreen());
//        }
//
//    return Scaffold(
//      appBar: AppBar(title: Text("Teacher Screen"),),
//      body: Container(
//        child: Card(
//          elevation: 2.0,
//          child: Container(
//            child:
//            ,
//          ),
//        ),
//      ),
//    );
//  }
//}


        class TeacherHomeApp extends StatefulWidget {
        TeacherHomeApp({
        Key key,
        }) : super(key: key);

        @override
        _TeacherHomeAppState createState() => _TeacherHomeAppState();
        }

        class _TeacherHomeAppState extends State<TeacherHomeApp> {
        final _formKey = GlobalKey<FormState>();
        String _email, _password;
        bool _isObscured = true;
        Color _eyeButtonColor = Colors.grey;
        Future<String> _message = Future<String>.value('');




        @override
        Widget build(BuildContext context) {
        return Container(
        child:  new StoreConnector<AppState, AddUserModel>(
        converter: (store) {
        return AddUserModel(
        addUserState: store.state.addUserState,
        loadingState: store.state.loadingState,
          user: store.state.user,
        );
        },
        builder: (BuildContext context, AddUserModel au) {
          var name = au.user.UserName.text;

          var User = au.user;

//          User.uiid.text ;
//          User.UserName.text ;
//          User.gender.text ;
//          User.email.text ;
//          User.phone.text ;
//          User.address.text ;
//          User.city.text ;
//          User.DOB.text ;
//          User.state.text ;
//          User.age.text ;
//          User.date_register.text ;
//          User.imageUri.text ;
//          User.Occupation.text ;
//          // EduCareId: datasnapshot.data['EduCareId'],
//          User.EduCareId.text ;
          Padding buildTitle(String txt) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                txt,
                style: TextStyle(fontSize: 42.0),
              ),
            );
          }

          Padding buildTitleLine() {
            return Padding(
              padding: const EdgeInsets.only(top: 4.0, left: 12.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  width: 38.0,
                  height: 1.5,
                  color: Colors.black,
                ),
              ),
            );
          }

        return new Scaffold(

          appBar: AppBar(title: Text("Teachers Screen"),),
          body: Container(
            child: Column(
              children: <Widget>[
                buildTitle('Teacher Screen'),
                buildTitleLine(),
                CardSettings(
                  contentAlign: TextAlign.start,
                  labelAlign: TextAlign.start,
                  children: <Widget>[
                    Text("Name :" + User.UserName.text),
                    Text("gender :" + User.gender.text),
                    Text("email :" + User.email.text),
                    Text("phone :" + User.phone.text),
                    Text("address :" + User.address.text),
                    Text("city :" + User.city.text),


                    Text("DOB :" + User.DOB.text),
                    Text("state :" + User.state.text),
                    Text("age :" + User.age.text),
                    Text("date_register :" + User.date_register.text),
                    Text("imageUri :" + User.imageUri.text),
                    Text("Occupation :" + User.Occupation.text),
                  ],
                ),
              ],
            ),
          ),

        );
        },
        ));
        }
        }