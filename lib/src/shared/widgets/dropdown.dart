part of app.widgets;

class DropDown {
  static Widget dropdown(
      String label, List<String> items, Function onChange, String value,
      {bool required = false}) {
    return new CardSettingsListPicker(
      options: items,
      onChanged: onChange,
      validator: (value) {
        if (required) {
          if (!items.contains(value) || value == "Select one") {
            return label + " is required";
          }
        }
      },
      initialValue: value,
      key: new GlobalKey(debugLabel: label + "-dropdown-key"),
      label: label,
      requiredIndicator:
          required ? Text('*', style: TextStyle(color: Colors.red)) : null,
    );
  }
}
