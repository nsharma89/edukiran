part of app.widgets;

// A

class prefrencePage extends StatefulWidget {
  prefrencePage({
    Key key,
  }) : super(key: key);

  @override
  _prefrencePageState createState() => _prefrencePageState();
}

class _prefrencePageState extends State<prefrencePage> {






  Padding buildTitle(String txt) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        txt,
        style: TextStyle(fontSize: 42.0),
      ),
    );
  }

  Padding buildTitleLine() {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0, left: 12.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          width: 38.0,
          height: 1.5,
          color: Colors.black,
        ),
      ),
    );
  }




  Align buildLoginButton(BuildContext context, AddUserModel au , String path) {
    return Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: () {
            au.VerifyeViewModel.onPressedCallback();
            Navigator.pushNamed(context, path);
          },
          color: Colors.grey[900],
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Verify',
            style: Theme.of(context).primaryTextTheme.button,
          ),
        ),
      ),
    );
  }

  Align buildSubmitButton(BuildContext context, AddUserModel au) {
    return Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: () {
            au.onSave();
          },
          color: Colors.grey[900],
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Submit',
            style: Theme.of(context).primaryTextTheme.button,
          ),
        ),
      ),
    );
  }





  Align buildOrText() {
    return Align(
      alignment: Alignment.center,
      child: Text(
        'or login with',
        style: TextStyle(fontSize: 12.0, color: Colors.grey),
      ),
    );
  }

  Container buildSocialMediaButtons(IconData icon, Color iconColor) {
    return Container(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, "/SignUp");
        },
        borderRadius: BorderRadius.circular(30.0),
        child: Icon(icon, color: iconColor),
      ),
      height: 46.0,
      width: 46.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.grey, width: 0.5)),
    );
  }

  Align buildSignUpText(BuildContext context, String path) {
    return Align(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, path);
        },
        borderRadius: BorderRadius.circular(30.0),
        child: RichText(
          text: TextSpan(
              text: 'You Don\'t have an account?',
              style: TextStyle(fontSize: 12.0, color: Colors.grey),
              children: <TextSpan>[
                TextSpan(
                    text: ' SIGN UP',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                        color: Colors.black)),
              ]),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        child:  new StoreConnector<AppState, AddUserModel>(
          converter: (store) {
            return AddUserModel(
              addUserState: store.state.addUserState,
              loadingState: store.state.loadingState,
              onSave: () {
                store.dispatch(AddingPrefrence());
              },
              addSubject: (v){
                store.dispatch(AddingSubjectPre(v));
              }
            );
          },
          builder: (BuildContext context, AddUserModel au) {
            return new Scaffold(
              body: Form(
                child: ListView(
                  padding: const EdgeInsets.fromLTRB(
                      22.0, 0.0, 22.0, 22.0),
                  children: <Widget>[
                    SizedBox(height: kToolbarHeight),
                    buildTitle('Please select your Preference'),
                    buildTitleLine(),
                    SizedBox(
                      height: 10.0,
                    ),
                    CardSettings(
                      labelAlign: TextAlign.left,
                      labelPadding: 0.0,
                      contentAlign: TextAlign.left,
                      children: <Widget>[
                        SizedBox(
                          height: 30.0,
                        ),
//                        Padding(
//                          padding: const EdgeInsets.only(left: 16.0,right: 16.0),
//                          child: TextFormField(
//
//                            decoration: InputDecoration(
//                              labelText: 'PreFrence',
//                            ),
//                            controller: au.addUserState.smsCodeController,
//                            keyboardType: TextInputType.number,
//
//                          ),
//                        ),

                        CardSettingsMultiselect(
                          options: ["English","Hindi","Malyalum",
                          "Spanish","France","AlienVoice"
                          ],
                          label: "Language",
                          initialValues: ["English","France","AlienVoice",
                          ],
                          onChanged: (v){},
                          onSaved: (v){
                            au.addSubject(v);
                          },

                        ),


                        SizedBox(
                          height: 20.0,
                        ),
                        CardSettingsMultiselect(
                          options: ["Mathematics","Language Arts","Science",
                          "Physical Education","Art","Music",
                          "Instrumental Music – specific instrument","Movement or Eurythmy","Dance","Leadership",
                          "Speech","Pre-algebra",
                          ],
                          label: "Subjects",
                          initialValues: ["Mathematics","Art","Music",
                          "Movement or Eurythmy","Speech","Leadership"],
                          onChanged: (v){
                            au.addSubject(v);
                          },


                        ),


                        SizedBox(
                          height: 60.0,
                        ),
                        buildSubmitButton(context,au),
                        SizedBox(
                          height: 40.0,
                        ),
                      ],
                    ),


                    SizedBox(
                      height: 16.0,
                    ),

                    SizedBox(height: 70.0),

                  ],
                ),
              ),
            );
          },
        ));
  }
}



