part of app.middlewares;

// https://flutterbyexample.com/firebase-log-in-middleware
const jsonCodec = const JsonCodec();
class LoginMiddleware implements MiddlewareClass<AppState> {
  String verificationId;
  final FirebaseAuth _auth = FirebaseAuth.instance;



  Future uploadFile( youtube.YoutubeApi api,
      String file,
      String title,
      String description,
      List<String> tags,
      String categoryId,
      String channelId,
      Store store
      ) {
    // We create a `Media` object with a `Stream` of bytes and the length of the
    // file. This media object is passed to the API call via `uploadMedia`.
    // We pass a partially filled-in `drive.File` object with the title we want
    // to give our newly created file.
    print("1");
    var localFile = new File(file);
    print("2");
    var media = new youtube.Media(localFile.openRead(), localFile.lengthSync());

//    var driveFile = new youtube.Video()..snippet.title = "asdf"
//                                    ..snippet.description =  "asdfdsf"
//                                    ..snippet.tags = ["adf","adf"]
//                                    ..snippet.categoryId = "27"
//                                    ..snippet.channelId = channelId
//                                    ..status.privacyStatus = "private"
//                                    ..toJson()
//    ;



    var driveFile = new youtube.Video()
    ;

    driveFile.snippet = new youtube.VideoSnippet();
    driveFile.snippet.title = title;
    driveFile.snippet.description = description;
    driveFile.snippet.tags = tags;
    driveFile.snippet.categoryId = categoryId;
    driveFile.snippet.channelId = channelId;


//    driveFile.status = new youtube.VideoStatus();
//    driveFile.status.privacyStatus = "public";



//    var snipet = driveFile.snippet;
//    snipet.title = "asdfds";



    print("3");
//    var driveFile = new youtube.Video();
      var part = "snippet";
    print("4");

//    return api.channels.list("id",forUsername: "YouTubeDev").then((videoresponse){
//      print('Uploaded $file. Id: ${videoresponse.toString()}');
//    });

    return api.videos.insert(driveFile ,part ,uploadMedia: media ).then((youtube.Video Video) {
      print('Uploaded $file. Id: ${Video.id}');
      var s = Video.id;

      Firestore.instance.collection('videoData').document(Video.id).setData({
        'Name':   title,
        'Description': description,
        'channelId': channelId,
        'url': "https://www.youtube.com/watch?v="+Video.id,
      });
      store.dispatch(vedioList());
    });
  }

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async {


    // Actions are classes, so you can Typecheck them
    if (action is GoogleLogInAction) {
      try {
        FirebaseUser user;
        // Firebase 'instances' are temporary instances which give
        // you access to your FirebaseUser. This includes
        // some tokens we need to sign in.
        final FirebaseAuth _auth = FirebaseAuth.instance;

        // GoogleSignIn is a specific sign in class.
        final GoogleSignIn _googleSignIn = new GoogleSignIn();

        // Try to sign in the user.
        // This method will either log in a user that your Firebase
        // is aware of, or it will prompt the user to log in
        // if its the first time.
        //
        GoogleSignInAccount googleUser = await _googleSignIn.signIn();
        GoogleSignInAuthentication googleAuth = await googleUser.authentication;

        // After checking for authentication,
        // We wil actually sign in the user
        // using the token that firebase.
        user = await _auth.signInWithGoogle(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );

        print('Logged in ' + user.displayName);

        // This can be tough to reason about -- or at least it was for me.
        // We're going to dispatch a new action if we logged in,
        //
        // We also continue the current cycle below by calling next(action).
        store.dispatch(new LogInSuccessful(user: user));
      } catch (error) {
        TutSnackbar.error(text: error);
        store.dispatch(new LogInFail(error));
        store.dispatch(new errorScreen(error:error));

      }
    }
//    if (action is FacebookLogInAction) {
//      try {
//        final FirebaseAuth _auth = FirebaseAuth.instance;
//        final FacebookLogin facebookLogin = new FacebookLogin();
//        final FacebookLoginResult result = await facebookLogin.logInWithReadPermissions(['email']);
//        final FirebaseUser user = await _auth.signInWithFacebook(
//          accessToken: result.accessToken.toString(),
//        );
//        print('Logged in ' + user.displayName);
//
//        store.dispatch(new LogInSuccessful(user: user));
//      } catch (error) {
//        store.dispatch(new LogInFail(error));
//      }
//    }

    if (action is UserAdded) {
      try {
        var  u = store.state.firebaseUser.uid;
        Firestore.instance.collection('Users').document(u).setData({
          'id': u,
          'UserName': store.state.addUserState.UserNameController.text,
          'gender': store.state.addUserState.Gender,
          'email': store.state.addUserState.emailController.text,
          'phone': store.state.addUserState.PhoneNumController.text,
          'address': store.state.addUserState.AddressController.text,
          'city':  store.state.addUserState.CityController.text,
          'DOB': store.state.addUserState.DOBController.text,
          'state': store.state.addUserState.StateController.text,
          'age':   "age",
          'date_register': "date_register",
          'imageUri': "imageUri",
          'Occupation': store.state.addUserState.Occupation,
        });

        store.dispatch(new prefrenceScreen());
      } catch (error) {
        store.dispatch(new LogInFail(error));
        store.dispatch(new errorScreen(error:error));

      }
    }

    if (action is AddingPrefrence) {
      try {
        var  u = store.state.firebaseUser.uid;
        Firestore.instance.collection('Users').document(u).collection("Prefrence").document("Subjects").setData({
          'Subjects': store.state.addUserState.SubjectPrefrence,
        });

        store.dispatch(new fetchUserData());
      } catch (error) {
        store.dispatch(new LogInFail(error));
        store.dispatch(new errorScreen(error:error));

      }
    }

    if (action is PhoneLogInAction) {
      try {


        String testSmsCode ;

        final PhoneVerificationCompleted verificationCompleted =
            (FirebaseUser user) {
              store.dispatch(new NavigateRegisterAction());
              TutSnackbar.success("Verified");
              Future<String>.value('signInWithPhoneNumber auto succeeded: $user');
          ;
        };

        final PhoneVerificationFailed verificationFailed =
            (AuthException authException) {
              TutSnackbar.success("Wrong Otp");
           Future<String>.value(
            'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
            ;
        };

        final PhoneCodeSent codeSent =
            (String verificationIda, [int forceResendingToken]) async {
          verificationId = verificationIda;
          store.state.addUserState.smsCodeController.text = testSmsCode;
        };

        final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
            (String verificationIda) {
          verificationId = verificationIda;
          store.state.addUserState.smsCodeController.text = testSmsCode;
        };

        await _auth.verifyPhoneNumber(
            phoneNumber: "+91" + store.state.addUserState.PhoneNumController.text,
            timeout: const Duration(seconds: 60  ),
            verificationCompleted: verificationCompleted,
            verificationFailed: verificationFailed,
            codeSent: codeSent,
            codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);

      } catch (error) {
        store.dispatch(new LogInFail(error));
        store.dispatch(new errorScreen(error:error));

      }
    }

    if (action is PhoneVerificationLogInAction) {
      try {
        final FirebaseUser user = await _auth.signInWithPhoneNumber(
          verificationId: verificationId,
          smsCode: store.state.addUserState.smsCodeController.text,
        );

        final FirebaseUser currentUser = await _auth.currentUser();
        assert(user.uid == currentUser.uid);

        store.state.addUserState.smsCodeController.text = '';


        var db = new DatabaseHelper();
        await db.saveUser(user.uid.toLowerCase());

        store.dispatch(new LogInSuccessful(user: user));
        store.dispatch(new fetchUserData());

      } catch (error) {
        store.dispatch(new LogInFail(error));
        store.dispatch(new errorScreen(error:error));

      }
    }

    if (action is fetchUserData) {
      try {
        final FirebaseAuth _auth = FirebaseAuth.instance;
        final FirebaseUser currentUser = await _auth.currentUser();

        if(currentUser == null){
          store.dispatch(new NavigateLoginPageAction());
        }

        print("fetchUserData");
        var  u = currentUser.uid;
        print("After fetchUserData");
        Firestore.instance.collection('Users').document(u).get().then((datasnapshot){
          if (datasnapshot.exists) {
               var User = store.state.user;

                   User.uiid.text = datasnapshot.data['id'];
               User.UserName.text = datasnapshot.data['UserName'];
               User.gender.text = datasnapshot.data['gender'];
               User.email.text = datasnapshot.data['email'];
               User.phone.text = datasnapshot.data['phone'];
               User.address.text = datasnapshot.data['address'];
               User.city.text = datasnapshot.data['city'];
               User.DOB.text = datasnapshot.data['DOB'];
               User.state.text = datasnapshot.data['state'];
               User.age.text = datasnapshot.data['age'];
               User.date_register.text = datasnapshot.data['date_register'];
               User.imageUri.text = datasnapshot.data['imageUri'];
               User.Occupation.text = datasnapshot.data['Occupation'];
          // EduCareId: datasnapshot.data['EduCareId'],
               User.EduCareId.text = "abc";




               if(datasnapshot.data['Occupation'].contains("Teacher") ){
                 store.dispatch(new TeacherHomeAppScreen());
                }else if(datasnapshot.data['Occupation'].contains("SuperAdmin") ){
                 store.dispatch(new SuperAdminScreen());
                 }else if(datasnapshot.data['Occupation'].contains("pStudent") ){
                 store.dispatch(new paidStudentScreen());
               }else if(datasnapshot.data['Occupation'].contains("Student") ){
                 store.dispatch(new normalStudentScreen());
               }


               //store.dispatch(new LoadingActionScreen());
          print("success");
          }else{
            store.dispatch(new NavigateRegisterAction());
          }
        });

      } catch (error) {
        store.dispatch(new LogInFail(error));
        store.dispatch(new errorScreen(error:error));

      }
    }

    if (action is AddingVedio) {
      try {

//        YoutubeApi _youtubeApi ;
//
//        var Vidio_file_format = "video/*";
//        var SAMPLE_VIDEO_FILENAME = "sample-video.mp4";
//
//        VideosResourceApi a ;
//        a.insert(request, part,);
//        final FirebaseAuth _auth = FirebaseAuth.instance;
//
//        final _googleSignIn = new GoogleSignIn();
//
//        await _googleSignIn.signIn();
//
//        final authHeaders = _googleSignIn.currentUser.authHeaders;

//        Client httpClient = new GoogleHttpClient(authHeaders);
        print("signinfailed");

//        final _credentials = new ServiceAccountCredentials.fromJson(r'''
//                            {
//                              "private_key_id": ...,
//                              "private_key": ...,
//                              "client_email": ...,
//                              "client_id": ...,
//                              "type": "service_account"
//                            }
//                            ''');
//

        var identifier = new ClientId("1014585071148-q3j85ohuikfljig9lns9bmsb6j76cou3.apps.googleusercontent.com", null);

        final _SCOPES = [youtube.YoutubeApi.YoutubeUploadScope];



//      var api = new youtube.YoutubeApi(new Client());
//        print("apifailed");
//      uploadFile(api,
//          store.state.addYoutubeVedio.filePath,
//          store.state.addYoutubeVedio.TitleController.text,
//          store.state.addYoutubeVedio.descriptionController.text,
//          ["",""],
//          store.state.addYoutubeVedio.categoryIdController,
//          "UCDSw8c5sfR49AP2rlnsagsw",
//      );


//        clientViaServiceAccount(_credentials, _SCOPES).then((http_client) {
//          var api = new youtube.YoutubeApi(http_client);
//          uploadFile(api,
//            store.state.addYoutubeVedio.filePath,
//            store.state.addYoutubeVedio.TitleController.text,
//            store.state.addYoutubeVedio.descriptionController.text,
//            ["",""],
//            store.state.addYoutubeVedio.categoryIdController,
//            "UCDSw8c5sfR49AP2rlnsagsw",
//          );
//
//
//
//        });


//        clientViaUserConsent(identifier, _SCOPES, userPrompt).then((client) {
//          var api = new youtube.YoutubeApi(client);
//          uploadFile(api,
//            store.state.addYoutubeVedio.filePath,
//            store.state.addYoutubeVedio.TitleController.text,
//            store.state.addYoutubeVedio.descriptionController.text,
//            ["",""],
//            store.state.addYoutubeVedio.categoryIdController,
//            "UCDSw8c5sfR49AP2rlnsagsw",
//          );
//
//
//
//        }).catchError((error) {
//          if (error is UserConsentException) {
//            print("You did not grant access :(");
//          } else {
//            print("An unknown error occured: $error");
//          }
//        });


        String apikey = "AIzaSyDZDGfoiIepe728no8EIkVXW1BXmsu5cLY" ;

        Client client1 = clientViaApiKey(apikey);

//        var api = new youtube.YoutubeApi(client1);
//        uploadFile(api,
//          store.state.addYoutubeVedio.filePath,
//          store.state.addYoutubeVedio.TitleController.text,
//          store.state.addYoutubeVedio.descriptionController.text,
//          ["",""],
//          store.state.addYoutubeVedio.categoryIdController,
//          "UCDSw8c5sfR49AP2rlnsagsw",
//        );
        FirebaseUser user;
        final FirebaseAuth _auth = FirebaseAuth.instance;




        final _googleSignIn = new GoogleSignIn(
            scopes: ['https://www.googleapis.com/auth/youtube.upload',
            'https://www.googleapis.com/auth/youtube',
              'https://www.googleapis.com/auth/youtube.force-ssl',
              'https://www.googleapis.com/auth/youtube.readonly',
              'https://www.googleapis.com/auth/youtubepartner',
                'https://www.googleapis.com/auth/youtubepartner-channel-audit'
            ]
    );

        GoogleSignInAccount googleUser = await _googleSignIn.signIn(

        );
        GoogleSignInAuthentication googleAuth = await googleUser.authentication;

        user = await _auth.signInWithGoogle(
          accessToken: googleAuth.accessToken,
          idToken: googleAuth.idToken,
        );

        final authHeaders = await  _googleSignIn.currentUser.authHeaders.then((result) async {
          final httpClient = new GoogleHttpClient(result);

//          obtainAccessCredentialsViaCodeExchange(httpClient ,identifier , "" ).then((onValue){
//            var api = new youtube.YoutubeApi(onValue);
//            uploadFile(api,
//              store.state.addYoutubeVedio.filePath,
//              store.state.addYoutubeVedio.TitleController.text,
//              store.state.addYoutubeVedio.descriptionController.text,
//              ["",""],
//              store.state.addYoutubeVedio.categoryIdController,
//              "UCDSw8c5sfR49AP2rlnsagsw",
//            );
//
//          });

          var api = new youtube.YoutubeApi(httpClient);

          print("yahan tak ho gaya");
         var a =  await uploadFile(api,
            store.state.addYoutubeVedio.filePath,
            store.state.addYoutubeVedio.TitleController.text,
            store.state.addYoutubeVedio.descriptionController.text,
            ["",""],
            store.state.addYoutubeVedio.categoryIdController,
            "UCDSw8c5sfR49AP2rlnsagsw",
           store
          );
         print(a);
        });;

//        final httpClient = new GoogleHttpClient(authHeaders);
//
//        var api = new youtube.YoutubeApi(httpClient);
//        uploadFile(api,
//          store.state.addYoutubeVedio.filePath,
//          store.state.addYoutubeVedio.TitleController.text,
//          store.state.addYoutubeVedio.descriptionController.text,
//          ["",""],
//          store.state.addYoutubeVedio.categoryIdController,
//          "UCDSw8c5sfR49AP2rlnsagsw",
//        );







        print("uploadfailed");





      } catch (error) {
        print("failed Login ");
        store.dispatch(new LogInFail(error));
        store.dispatch(new errorScreen(error:error));

      }
    }

    next(action);
  }
}

class LogoutMiddleware implements MiddlewareClass<AppState> {
  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async {
    if (action is LogOut) {
      final FirebaseAuth _auth = FirebaseAuth.instance;
      try {
        await _auth.signOut();
        print('logging out...');
        store.dispatch(new LogOutSuccessful());
      } catch (error) {
        print(error);
      }
    }
    next(action);
  }
}

// ACTIONS

class GoogleLogInAction {}

class PhoneLogInAction {}

class UserAdded {}

class fetchUserData {}

class PhoneVerificationLogInAction {}

class FacebookLogInAction {}

class UserPwdLogInAction {}

class AddingPrefrence{}

class AddingVedio{}

class LogInSuccessful {
  final FirebaseUser user;

  LogInSuccessful({@required this.user});

  @override
  String toString() {
    return 'LogInSuccessful{user: $user}';
  }
}

class LogInFail {
  final dynamic error;
  LogInFail(this.error);
  @override
  String toString() {
    return 'LogInFail{There was an error loggin in: $error}';
  }
}

class LogOut {}

class LogOutSuccessful {
  LogOutSuccessful();
  @override
  String toString() {
    return 'LogOutSuccessful{user: null}';
  }
}

AppState _logIn(AppState state, LogInSuccessful action) {
  return state.copyWith(firebaseUser: action.user);
}

AppState _logOut(AppState state, LogOutSuccessful action) {
  return state.copyWith(disconnect: true);
}


