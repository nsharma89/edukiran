part of app.widgets;

class AuthorScreen extends StatefulWidget {
  AuthorScreen({
    Key key,
  }) : super(key: key);

  @override
  _AuthorScreenState createState() => _AuthorScreenState();
}

class _AuthorScreenState extends State<AuthorScreen> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static GlobalKey<ScaffoldState> _ScaffoldStatekey =
      GlobalKey<ScaffoldState>();

  String _filePath;

  Widget addtextfield(
      String LabelText, IconData IconS, TextEditingController controller,
      {Function validator, int Maxlines}) {
    return Padding(
      padding: const EdgeInsets.only(left: 16.0, right: 16.0),
      child: TextFormField(
        decoration: InputDecoration(
          labelText: LabelText,
          prefixIcon: Icon(IconS),
        ),
        controller: controller,
        keyboardType: TextInputType.text,
        validator: validator,
        maxLines: Maxlines,
      ),
    );
  }

  void getFilePath(AddAuthorModel au) async {
    try {
      File filePath = await ImagePicker.pickVideo(source: ImageSource.gallery);
      if (filePath == '') {
        return;
      }

      print("File path: " + filePath.path);
      setState(() {
        this._filePath = filePath.path;
        au.anything1();
      });
    } on PlatformException catch (e) {
      print("Error while picking the file: " + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, AddAuthorModel>(
      converter: (store) {
        return AddAuthorModel(
          addAuthor: store.state.addAuthor,
          onSave: () {
            if (_formKey.currentState.validate()) {
              store.dispatch(refreshScreen1());
              store.dispatch(AuthorAdded());
            } else {
              _ScaffoldStatekey.currentState.showSnackBar(
                  TutSnackbar.error(text: "Please Enter the Values to field"));
            }
          },
          anything1: () {
            store.dispatch(addImagePath(_filePath));
            print("ok");
          },
          navigate: (s) {
            if (s == "addVedio") {
              store.dispatch(advScreen());
            }
            if (s == "usm") {
              store.dispatch(UserManagementScreen());
            }
            if (s == "aly") {
              store.dispatch(anlyScreen());
            }

            if (s == "CScree") {
              store.dispatch(CScreen());
            }
            if (s == "Author") {
              store.dispatch(AuthorScreen1());
            }
            if (s == "Course") {
              store.dispatch(CourseScreen1());
            }

            if (s == "set") {
              store.dispatch(SettingsScreen());
            }
            if (s == "prof") {
              store.dispatch(profileScreen1());
            }
          },
        );
      },
      builder: (BuildContext context, AddAuthorModel au) {
        return Scaffold(
          key: _ScaffoldStatekey,
          body: CustomScrollView(
            primary: false,
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 150.0,
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: IconButton(
                      icon: Icon(Icons.perm_identity),
                      onPressed: () => au.navigate("prof"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: IconButton(
                      icon: Icon(Icons.settings),
                      onPressed: () => au.navigate("set"),
                    ),
                  ),
                ],
                backgroundColor: Colors.black45,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text('Add Author'),
                  background: Image.asset(
                    "asset/fg2.jpg",
                    fit: BoxFit.fitWidth,
                    height: 150.0,
                  ),
                  collapseMode: CollapseMode.parallax,
                ),
              ),
              SliverPadding(
                  padding: const EdgeInsets.only(
                      left: 5.0, right: 5.0, bottom: 20.0, top: 20.0),
                  sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                    (builder, index) {
                      return CardSettings(
                        labelAlign: TextAlign.left,
                        labelPadding: 0.0,
                        contentAlign: TextAlign.left,
                        children: <Widget>[
                          SizedBox(
                            height: 30.0,
                          ),
                          Container(
                            child: Center(
                              child: new InkWell(
                                onTap: () {
                                  getFilePath(au);
                                },
                                child: new Padding(
                                  padding: EdgeInsets.all(16.0),
                                  child: au.addAuthor.photoPath == ""
                                      ? new Container(
                                          width: 200.0,
                                          height: 200.0,
                                          child: Center(
                                              child: new Text("Take a picture",
                                                  style: TextStyle(
                                                      color: Colors.blue))),
                                          decoration: new BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.transparent,
                                            border: new Border.all(
                                              color: Colors.deepPurple,
                                              width: 2.0,
                                            ),
                                          ),
                                        )
                                      : new Container(
                                          width: 200.0,
                                          height: 200.0,
                                          decoration: new BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: new DecorationImage(
                                              fit: BoxFit.cover,
                                              image: new FileImage(new File(
                                                  au.addAuthor.photoPath)),
                                            ),
                                          )),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 30.0,
                          ),
                          Form(
                              key: _formKey,
                              child: Column(
                                children: <Widget>[
                                  addtextfield("Name", Icons.account_circle,
                                      au.addAuthor.NameController,
                                      validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Author name ';
                                    }
                                  }),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  addtextfield("Qualification", Icons.book,
                                      au.addAuthor.QualificationController,
                                      validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Qualification';
                                    }
                                  }),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  addtextfield("Details", Icons.list,
                                      au.addAuthor.descriptionController,
                                      validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter Details';
                                    }
                                  }, Maxlines: 3),
                                  SizedBox(
                                    height: 10.0,
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                  Align(
                                    child: SizedBox(
                                      height: 50.0,
                                      width: 200.0,
                                      child: FlatButton(
                                        onPressed: au.onSave,
                                        color: Colors.deepPurple,
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(30.0)),
                                        child: Text(
                                          'Add Author',
                                          style: TextStyle(
                                            color: Colors.white,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 30.0,
                                  ),
                                ],
                              )),
                        ],
                      );
                    },
                    childCount: 1,
                  ))),
            ],
          ),
        );
      },
    );
    ;
  }
}

class AddAuthorModel {
  final AddAuthor addAuthor;

  final void Function(dynamic s) onOccupationStudent;
  final void Function(dynamic s) onOccupationTEacher;
  final void Function(dynamic s) navigate;
  final void Function() anything1;
  final _VerifyeViewModel VerifyeViewModel;
  final AppState appState;

  final void Function() onSave;
  final void Function(
    String name,
    String a,
    String b,
  ) showProfile;

  AddAuthorModel(
      {this.addAuthor,
      this.onOccupationStudent,
      this.onOccupationTEacher,
      this.onSave,
      this.VerifyeViewModel,
      this.appState,
      this.navigate,
      this.anything1,
      this.showProfile});
}
