part of app.widgets;

class pPlayer extends StatefulWidget {
  @override
  VideoState createState() => VideoState();
}

class VideoState extends State<pPlayer> {
  VideoPlayerController playerController;
  VoidCallback listener;

  @override
  void initState() {
    super.initState();
    listener = () {
      setState(() {});
    };
  }

  void createVideo() {
    if (playerController == null) {
      playerController = VideoPlayerController.network(
          "https://flutter.github.io/assets-for-api-docs/videos/butterfly.mp4")
        ..addListener(listener)
        ..setVolume(1.0)
        ..initialize()
        ..play();
    } else {
      if (playerController.value.isPlaying) {
        playerController.pause();
      } else {
        playerController.initialize();
        playerController.play();
      }
    }
  }

  @override
  void deactivate() {
    playerController.setVolume(0.0);
    playerController.removeListener(listener);
    super.deactivate();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new StoreConnector<AppState, AddAuthorModel>(
      converter: (store) {
        return AddAuthorModel(
            addAuthor: store.state.addAuthor,
            showProfile: (name, dis, qal) {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        uiProfile(name: name, discription: dis, Qual: qal)),
              );
//            store.dispatch(uiProfile1(name: name , discription: dis, Qual: qal));
            });
      },
      builder: (BuildContext context, AddAuthorModel au) {
        return Scaffold(
          body: CustomScrollView(
            primary: false,
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 240.0,
                backgroundColor: Colors.black45,
                flexibleSpace: FlexibleSpaceBar(
                  background: Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Center(
                      child: Chewie(
                        new VideoPlayerController.network(
                            'https://wehelptechnology.com/wp-content/uploads/2018/11/Motorway-18082.mp4'),
                        aspectRatio: 3 / 2,
                        autoPlay: true,
                        looping: true,
                        showControls: true,
                      ),
                    ),
                  ),
                  collapseMode: CollapseMode.parallax,
                ),
              ),
              SliverPadding(
                  padding: const EdgeInsets.only(
                      left: 5.0, right: 5.0, bottom: 20.0, top: 20.0),
                  sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                    (builder, index) {
                      return Container(
                        height: 500.0,
                        child: StreamBuilder(
                          stream: Firestore.instance
                              .collection('authorData')
                              .snapshots(),
                          builder: (BuildContext context,
                              AsyncSnapshot<QuerySnapshot> snapshot) {
                            if (!snapshot.hasData)
                              return CircularProgressIndicator();
                            return CategoryLIst(
                              documents: snapshot.data.documents,
                              au: au,
                            );
                          },
                        ),
                      );
                    },
                    childCount: 1,
                  ))),
            ],
          ),
        );
      },
    );
    ;
  }

//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: AppBar(
//        title: Text('Video Example'),
//
//      ),
//      body: Center(
//          child: Chewie(
//        new VideoPlayerController.network(
//            'https://wehelptechnology.com/wp-content/uploads/2018/11/Motorway-18082.mp4'),
//        aspectRatio: 3 / 2,
//        autoPlay: true,
//        looping: true,
//        showControls: true,
//      )),
//      floatingActionButton: FloatingActionButton(
//        onPressed: () {
//          createVideo();
//          playerController.play();
//        },
//        child: Icon(Icons.play_arrow),
//      ),
//    );
//  }
}
