part of app.widgets;

//class SuperAdmin extends StatefulWidget {
//  @override
//  _SuperAdminState createState() => _SuperAdminState();
//}
//
//class _SuperAdminState extends State<SuperAdmin> {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold();
//  }
//}
class SuperAdmin extends StatefulWidget {
  SuperAdmin({
    Key key,
  }) : super(key: key);

  @override
  _SuperAdminState createState() => _SuperAdminState();
}

class _SuperAdminState extends State<SuperAdmin> {
  final _formKey = GlobalKey<FormState>();
  String _email, _password;
  bool _isObscured = true;
  Color _eyeButtonColor = Colors.grey;
  Future<String> _message = Future<String>.value('');




  @override
  Widget build(BuildContext context) {
    return Container(
        child:  new StoreConnector<AppState, AddUserModel>(
          converter: (store) {
            return AddUserModel(
              addUserState: store.state.addUserState,
              loadingState: store.state.loadingState,
              user: store.state.user,
              navigate: (s) {
                if(s == "addVedio"){
                store.dispatch(advScreen());
                }
                if(s == "usm"){
                  store.dispatch(UserManagementScreen());
                }
                if(s == "aly"){
                  store.dispatch(anlyScreen());
                }

                if(s == "CScree"){
                  store.dispatch(CScreen());
                }
                if(s == "Author"){
                  store.dispatch(AuthorScreen1());
                }
                if(s == "Course"){
                  store.dispatch(CourseScreen1());
                }

                if(s == "set"){
                  store.dispatch(SettingsScreen());
                }
                if(s == "prof"){
                  store.dispatch(profileScreen1());
                }

                if(s == "authors"){
                  store.dispatch(authorprofile());
                }

                if(s == "vedioList"){
                  store.dispatch(vedioList());
                }



              },
            );
          },
          builder: (BuildContext context, AddUserModel au) {
            var name = au.user.UserName.text;

            var User = au.user;


            return new Scaffold(

              backgroundColor: Colors.white ,
              body: CustomScrollView(
                primary: false,
                scrollDirection: Axis.vertical,
                slivers: <Widget>[
                   SliverAppBar(
                    pinned: true,
                    expandedHeight: 250.0,
                    actions: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0 ,right:8.0 ),
                        child: IconButton(
                          icon: Icon(Icons.perm_identity),
                        onPressed: () => au.navigate("prof"),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0,right:8.0 ),
                        child: IconButton(
                          icon: Icon(Icons.settings),
                          onPressed: () => au.navigate("set"),
                        ),
                      ),

                    ],
                     backgroundColor: Colors.black45,
                    flexibleSpace: FlexibleSpaceBar(
                      title: Text('Welcome Admin'),
                      background: Image.asset("asset/SuperAdminScreen.jpg" ,fit: BoxFit.fitHeight,filterQuality: FilterQuality.low,colorBlendMode: BlendMode.darken,),
                      collapseMode: CollapseMode.parallax,
                    ),
                  ),
                  SliverPadding(
                    padding: const EdgeInsets.all(20.0),
                    sliver: SliverGrid.count(
                      crossAxisSpacing: 10.0,
                      crossAxisCount: 2,
                      mainAxisSpacing: 10.0,
                      children: <Widget>[
                        InkWell(
                          child:  Container(

                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Image.asset("asset/bck5.png",fit: BoxFit.fitHeight,height: 150.0),
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0,right:8.0,top: 8.0),
                                      child: Icon(Icons.category,color: Colors.white,size: 120.0,),
                                    ),
                                    Text("Add Category" ,style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold,wordSpacing: 2.0,letterSpacing: 2.0 ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          onTap: (){
                            au.navigate("CScree");
                          },
                        ),
                        InkWell(
                          child:  Container(

                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Image.asset("asset/bck7.png",fit: BoxFit.fitHeight,height: 150.0,filterQuality: FilterQuality.low,colorBlendMode: BlendMode.darken,),
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0,right:8.0,top: 8.0),
                                      child: Icon(Icons.book,color: Colors.white,size: 120.0,),
                                    ),
                                    Text("Add Course" ,style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold,wordSpacing: 2.0,letterSpacing: 2.0 ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          onTap: (){
                            au.navigate("Course");
                          },
                        ),
                        InkWell(
                          child:  Container(

                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Image.asset("asset/bck5.png",fit: BoxFit.fitHeight,height: 150.0),
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0,right:8.0,top: 8.0),
                                      child: Icon(Icons.group_add,color: Colors.white,size: 120.0,),
                                    ),
                                    Text("Add Author" ,style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold,wordSpacing: 2.0,letterSpacing: 2.0 ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          onTap: (){
                            au.navigate("Author");
                          },
                        ),
                        InkWell(
                          child:  Container(

                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Image.asset("asset/bck4.png",fit: BoxFit.fitHeight,height: 150.0,filterQuality: FilterQuality.low,colorBlendMode: BlendMode.darken,),
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0,right:8.0,top: 8.0),
                                      child: Icon(Icons.supervised_user_circle,color: Colors.white,size: 120.0,),
                                    ),
                                    Text("Authors" ,style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold,wordSpacing: 2.0,letterSpacing: 2.0 ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          onTap: (){
                            au.navigate("authors");
                          },
                        ),
                        InkWell(
                          child:  Container(

                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Image.asset("asset/bck4.png",fit: BoxFit.fitHeight,height: 150.0,),
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0,right:8.0,top: 8.0),
                                      child: Icon(Icons.add_a_photo,color: Colors.white,size: 120.0,),
                                    ),
                                    Text("ADD VEDIO" ,style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold,wordSpacing: 2.0,letterSpacing: 2.0 ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            decoration: ShapeDecoration(shape:
                                RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),),
                            foregroundDecoration: ShapeDecoration(shape:
                            RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),),
                          ),
                          onTap: (){
                            au.navigate("addVedio");
                          },
                        ),
                        InkWell(
                          child:  Container(

                            child: Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                Image.asset("asset/bck6.png",fit: BoxFit.fitHeight,height: 150.0,),
                                Column(
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 8.0,right:8.0,top: 8.0),
                                      child: Icon(Icons.vertical_align_bottom,color: Colors.white,size: 120.0,),
                                    ),
                                    Text("VIDEO LIST" ,style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold,wordSpacing: 2.0,letterSpacing: 2.0 ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            decoration: ShapeDecoration(shape:
                            RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),),
                            foregroundDecoration: ShapeDecoration(shape:
                            RoundedRectangleBorder(borderRadius: BorderRadius.circular(60.0)),),
                          ),
                          onTap: (){
                            au.navigate("vedioList");
                          },
                        ),


























                         InkWell(
                           child:  Container(

                             child: Stack(
                               alignment: Alignment.center,
                               children: <Widget>[
                                 Image.asset("asset/bck7.png",fit: BoxFit.fitHeight,height: 150.0,),
                                 Column(
                                   children: <Widget>[
                                     Padding(
                                       padding: const EdgeInsets.only(left: 8.0,right:8.0,top: 8.0),
                                       child: Icon(Icons.account_circle,color: Colors.white,size: 120.0,),
                                     ),
                                     Text("MANAGE USER" ,style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold,wordSpacing: 2.0,letterSpacing: 2.0 ),
                                     ),
                                   ],
                                 ),
                               ],
                             ),
                           ),
                           onTap: (){
                             au.navigate("usm");
                           },
                         ),
                         InkWell(
                           child:  Container(

                             child: Stack(
                               alignment: Alignment.center,
                               children: <Widget>[
                                 Image.asset("asset/bck5.png",fit: BoxFit.fitHeight,height: 150.0,),
                                 Column(
                                   children: <Widget>[
                                     Padding(
                                       padding: const EdgeInsets.only(left: 8.0,right:8.0,top: 8.0),
                                       child: Icon(Icons.graphic_eq,color: Colors.white,size: 120.0,),
                                     ),
                                     Text("Analaytic" ,style: TextStyle(color: Colors.white,fontSize: 16.0,fontWeight: FontWeight.bold,wordSpacing: 2.0,letterSpacing: 2.0 ),
                                     ),
                                   ],
                                 ),
                               ],
                             ),
                           ),
                           onTap: (){
                             au.navigate("aly");
                           },
                         ),



                      ],
                    ),
                  ),
                ],
              ),

            );
          },
        ));
  }
}