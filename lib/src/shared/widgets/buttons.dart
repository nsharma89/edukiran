part of app.widgets;

class GoogleAuthButtonContainer extends StatelessWidget {
  GoogleAuthButtonContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Connect to the store:
    return new StoreConnector<AppState, _GoogleViewModel>(
      converter: _GoogleViewModel.fromStore,
      builder: (BuildContext context, _GoogleViewModel vm) {
        // We haven't made this yet.
        return new GoogleAuthButton(
          buttonText: vm.buttonText,
          onPressedCallback: vm.onPressedCallback,
        );
      },
    );
  }
}

class _GoogleViewModel {
  final String buttonText;
  final Function onPressedCallback;

  _GoogleViewModel({this.onPressedCallback, this.buttonText});

  static _GoogleViewModel fromStore(Store<AppState> store) {
    return new _GoogleViewModel(onPressedCallback: () {
      if (store.state.firebaseUser != null) {
        store.dispatch(new LogOut());
      } else {
        store.dispatch(new GoogleLogInAction());
      }
    });
  }
}

class GoogleAuthButton extends StatelessWidget {
  final String buttonText;
  final Function onPressedCallback;

  // Passed in from Container
  GoogleAuthButton({
    @required this.buttonText,
    this.onPressedCallback,
  });

  @override
  Widget build(BuildContext context) {
    return new RaisedButton(
      onPressed: onPressedCallback,
      color: Colors.transparent,
      elevation: 0.0,
      child: Container(
        child: Icon(Icons.threesixty, color: Colors.black87),
        height: 46.0,
        width: 46.0,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.grey, width: 0.5)),
      ),
    );
  }
}

class FacebookAuthButtonContainer extends StatelessWidget {
  FacebookAuthButtonContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, _FacebookViewModel>(
      converter: _FacebookViewModel.fromStore,
      builder: (BuildContext context, _FacebookViewModel vm) {
        return new FacebookAuthButton(
          buttonText: vm.buttonText,
          onPressedCallback: vm.onPressedCallback,
        );
      },
    );
  }
}

class _FacebookViewModel {
  final String buttonText;
  final Function onPressedCallback;

  _FacebookViewModel({this.onPressedCallback, this.buttonText});

  static _FacebookViewModel fromStore(Store<AppState> store) {
    return new _FacebookViewModel(onPressedCallback: () {
      if (store.state.firebaseUser != null) {
        store.dispatch(new LogOut());
      } else {
        store.dispatch(new FacebookLogInAction());
      }
    });
  }
}

class FacebookAuthButton extends StatelessWidget {
  final String buttonText;
  final Function onPressedCallback;

  // Passed in from Container
  FacebookAuthButton({
    @required this.buttonText,
    this.onPressedCallback,
  });

  @override
  Widget build(BuildContext context) {
    // Raised button is a widget that gives some
    // automatic Material design styles
    return new RaisedButton(
      onPressed: onPressedCallback,
      elevation: 0.0,
      color: Colors.transparent,
      child: new Container(
        child: Icon(Icons.threesixty, color: Colors.black87),
        height: 46.0,
        width: 46.0,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: Colors.grey, width: 0.5)),
      ),
    );
  }
}

class TeddySlideTransitionRoute<T> extends MaterialPageRoute<T> {
  TeddySlideTransitionRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    return SlideTransition(
      position: new Tween<Offset>(
        begin: const Offset(1.0, 0.0),
        end: Offset.zero,
      ).animate(animation),
      child: new SlideTransition(
        position: new Tween<Offset>(
          begin: Offset.zero,
          end: const Offset(-1.0, 0.0),
        ).animate(secondaryAnimation),
        child: child,
      ),
    );
  }
}
