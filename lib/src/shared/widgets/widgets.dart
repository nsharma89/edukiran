library app.widgets;

import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:async';
import 'package:intl/intl.dart' as intl;

import 'package:flutter/services.dart';

import 'package:flutter_redux/flutter_redux.dart';

import 'package:redux/redux.dart';
import 'package:meta/meta.dart';

import 'package:card_settings/card_settings.dart';
import 'package:firebase_auth/firebase_auth.dart';
// Import state
import 'package:edu_kiran/src/shared/middleware/middlewares.dart';

// Import middleware
import 'package:edu_kiran/src/shared/state/appState.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:flutter_search_panel/flutter_search_panel.dart';
import 'package:image_picker/image_picker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';

// Import swagger

part 'package:edu_kiran/src/screens/Loading/view.dart';
part 'package:edu_kiran/src/screens/teacher/view.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/view.dart';
part 'package:edu_kiran/src/screens/paidStudent/state.dart';
part 'package:edu_kiran/src/screens/normalStudent/state.dart';
part 'package:edu_kiran/src/shared/widgets/buttons.dart';
part 'package:edu_kiran/src/screens/loginPage/view.dart';
part 'package:edu_kiran/src/shared/widgets/snackbar.dart';
part 'package:edu_kiran/src/screens/loginPage/otpview.dart';
part 'package:edu_kiran/src/screens/loginPage/register.dart';
part 'package:edu_kiran/src/screens/loginPage/prefrence.dart';
part 'package:edu_kiran/src/screens/Loading/splashScreen.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/Analytic.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/addVed.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/addAuthor.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/addCategory.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/addCourse.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/UserManage.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/setting.dart';
part 'package:edu_kiran/src/screens/SuperAdmin/profile.dart';
part 'package:edu_kiran/src/screens/profile/authorprofile.dart';
part 'package:edu_kiran/src/screens/profile/UIprofile.dart';
part 'package:edu_kiran/src/screens/profile/addedvid.dart';
part 'package:edu_kiran/src/screens/profile/refreshLoading.dart';
part 'package:edu_kiran/src/screens/profile/error Screen.dart';
part 'package:edu_kiran/src/screens/profile/CategoryPro.dart';
part 'package:edu_kiran/src/shared/widgets/dropdown.dart';
part 'package:edu_kiran/src/shared/widgets/player/portraitPlayer.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
