part of app.widgets;

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, LoadingScreenViewModel>(
        onInit: (store) {
          print("onInit");
          store.dispatch(fetchUserData());
        },
        converter: (store) {
          return LoadingScreenViewModel(
            loadingState: store.state.loadingState,
            disableUserInputAndShowLoadingAnimation:
                store.state.disableUserInputAndShowLoadingAnimation,
            preload: () {
              print("preload");
              store.dispatch(fetchUserData());},
          );
        },
        builder: (BuildContext context, LoadingScreenViewModel vm) {
//          Widget roleBasedHomeWidget = Container();
//            if (vm.user.Occupation.contains("SuperAdmin")) {
//              roleBasedHomeWidget = SuperAdmin();
//            } else if (vm.user.Occupation
//                .contains("teacher")) {
//              roleBasedHomeWidget = TeacherHomeApp();
//            }
//            if (vm.user.Occupation
//                .contains("normalStudent")) {
//              roleBasedHomeWidget = normalStudent();
//            } else if (vm.user.Occupation
//                .contains("paidStudent")) {
//              roleBasedHomeWidget = paidStudent();
//            }


//          if (vm.loadingState.HasPreloaded()) {
//            Stack stack = new Stack(fit: StackFit.expand, children: [
//              roleBasedHomeWidget,
//            ]);
//
//            if (vm.disableUserInputAndShowLoadingAnimation) {
//              stack = new Stack(
//                  fit: StackFit.expand,
//                  children: [roleBasedHomeWidget, new TeddySpinner()]);
//            }
//
//            return MaterialApp(
//              title: "Edu Tut",
//              home: stack,
//              locale: const Locale('en', 'US'), // English
//              theme: ThemeData(
//                accentColor: Colors.deepPurple[400], // used for card headers
//                cardColor: Colors.white, // used for field backgrounds
//                backgroundColor: Colors.indigo[100], // color outside the card
//                primaryColor: Colors.red, // color of page header
//                dividerColor: Colors.transparent,
//                buttonColor:
//                    Colors.lightBlueAccent[100], // background color of buttons
//                iconTheme: IconThemeData(color: Colors.pink),
//                textTheme: TextTheme(
//                  button: TextStyle(
//                    color: Colors.deepPurple[900],
//                  ), // style of button text// style of input text
//                ),
//              ),
//            );
//          }

//          if (vm.loadingState.HasError()) {
//            return new MaterialApp(
//              title: "Edu Tut",
//              home: Scaffold(
//                body: new Center(
//                    child: Column(
//                  children: <Widget>[
//                    new Text(vm.loadingState.GetErrors()),
//                  ],
//                )),
//              ),
//            );
//          }

          return new SplashScreen(
            seconds: 14,
            navigateAfterSeconds:  "sorry" ,
            title: new Text('Welcome In EduTuts',
              style: new TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 20.0
              ),),
            image: new Image.asset("asset/logo.png"),
            backgroundColor: Colors.white,
            styleTextUnderTheLoader: new TextStyle(),
            photoSize: 100.0,
            onClick: ()=>print("Flutter Egypt"),
            loaderColor: Colors.red,
          );
        });
  }
}



class LoadingScreenViewModel {
  final LoadingState loadingState;
  final User user;
  final bool isConnected;
  final bool disableUserInputAndShowLoadingAnimation;
  final Function preload;

  LoadingScreenViewModel({
    this.loadingState,
    this.isConnected,
    this.preload,
    this.disableUserInputAndShowLoadingAnimation,
    this.user
  });
}

class TeddySpinner extends StatelessWidget {
  TeddySpinner();

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: FractionalOffset.center,
      child: new AnimatedOpacity(
        duration: new Duration(seconds: 1),
        opacity: 0.5,
        child: new Container(
          color: Colors.grey,
          alignment: FractionalOffset.center,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }
}
