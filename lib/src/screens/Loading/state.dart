part of app.state;

class LoadingState {
  // User informations
  final MeState meState;

  // All kids state

  LoadingState({
    this.meState,
  });

  factory LoadingState.initial() {
    return new LoadingState(
      meState: MeState(),
    );
  }

  LoadingState copyWith({
    MeState meState,
  }) {
    return new LoadingState(
      meState: meState ?? this.meState,
    );
  }



  bool HasPreloaded() {
    return this.meState != null;
  }




}
