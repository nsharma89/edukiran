part of app.widgets;

class TutSnackbar {
  static SnackBar success(String text) {
    return SnackBar(
      content: new Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                bottom: 8.0, left: 8.0, right: 16.0, top: 8.0),
            child: Container(
                child: new Icon(Icons.check_circle, color: Colors.green)),
          ),
          Expanded(
              child:
                  Container(height: 60.0, child: Center(child: new Text(text))))
        ],
      ),
    );
  }

  static SnackBar error({String text = "Error(s) found. Please try again."}) {
    return SnackBar(
      content: new Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                bottom: 8.0, left: 8.0, right: 16.0, top: 8.0),
            child: Container(child: new Icon(Icons.error, color: Colors.red)),
          ),
          Expanded(
              child:
                  Container(height: 60.0, child: Center(child: new Text(text))))
        ],
      ),
    );
  }

  static SnackBar errorWithAction(
      String text, Function() onPressed, String actionText) {
    return SnackBar(
      content: new Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(
                bottom: 8.0, left: 8.0, right: 16.0, top: 8.0),
            child: Container(child: new Icon(Icons.error, color: Colors.red)),
          ),
          Expanded(
              child:
                  Container(height: 60.0, child: Center(child: new Text(text))))
        ],
      ),
      action:
          onPressed ?? SnackBarAction(label: actionText, onPressed: onPressed),
    );
  }
}
