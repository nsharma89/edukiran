part of app.state;

class AddCourseVedio {
  // Registration response

  final String CourseID;
  final String AuthorName;
  final List<String> ListVedeo;
  final String photoLink;
  final String Category;
  final String SubCategory;
  final List<String> LanguageTeaching;
  final bool isPaid;

  final List<String> tags;

  final TextEditingController TitleController;
  final TextEditingController descriptionController;

  AddCourseVedio({
    this.CourseID,
    this.AuthorName,
    this.ListVedeo,
    this.photoLink,
    this.TitleController,
    this.descriptionController,
    this.isPaid,
    this.tags,
    this.LanguageTeaching,
    this.Category,
    this.SubCategory,
  });

  factory AddCourseVedio.initial() {
    return new AddCourseVedio(
        CourseID:"",
        AuthorName:"",
         ListVedeo:[],
        photoLink:"",
        Category:"",
        SubCategory:"",
        LanguageTeaching:[],
        isPaid: false,


      TitleController: new TextEditingController(),
      descriptionController: new TextEditingController(),

      tags: [],
    );
  }

  AddCourseVedio copyWith({
     String CourseID,
     String AuthorName,
     List<String> ListVedeo,
     String photoLink,
     String Category,
     String SubCategory,
     List<String> LanguageTeaching,
     bool isPaid,

     List<String> tags,
  }) {
    return new AddCourseVedio(
      TitleController: this.TitleController,
      descriptionController: this.descriptionController,

      CourseID: CourseID != null && CourseID != "" ? CourseID : this.CourseID,
      AuthorName: AuthorName != null && AuthorName != "" ? AuthorName : this.AuthorName,
      ListVedeo: ListVedeo != null && ListVedeo != "" ? ListVedeo : this.ListVedeo,
      photoLink: photoLink != null && photoLink != "" ? photoLink : this.photoLink,
      Category: Category != null && Category != "" ? Category : this.Category,
      SubCategory: SubCategory != null && SubCategory != "" ? SubCategory : this.SubCategory,
      LanguageTeaching:LanguageTeaching != null && LanguageTeaching != "" ? LanguageTeaching : this.LanguageTeaching,
      isPaid: isPaid != null && isPaid != "" ? isPaid : this.isPaid,


      tags: tags != null && tags != "" ? tags : this.tags,
    );
  }
}

final addcoursereducer = combineReducers<AppState>([

]);
