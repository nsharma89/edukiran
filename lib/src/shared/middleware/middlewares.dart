library app.middlewares;

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import 'dart:io';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:edu_kiran/src/shared/widgets/widgets.dart';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:io' as io;
import 'package:path_provider/path_provider.dart';
import 'package:googleapis/youtube/v3.dart' as youtube;
import 'package:http/http.dart' show Client;
import 'package:googleapis_auth/auth_io.dart';
// Import states
import 'package:edu_kiran/src/shared/state/appState.dart';
import 'package:edu_kiran/src/shared/middleware/gapi.dart';
import 'package:firebase_storage/firebase_storage.dart';

part 'package:edu_kiran/src/shared/middleware/authanticate.dart';
part 'package:edu_kiran/src/shared/middleware/reducers.dart';
part 'package:edu_kiran/src/shared/middleware/keys.dart';
part 'package:edu_kiran/src/shared/middleware/me.dart';
part 'package:edu_kiran/src/shared/middleware/db.dart';
//data Middleware
part 'package:edu_kiran/src/shared/middleware/dataMiddleware.dart';

class NavigateMiddleware implements MiddlewareClass<AppState> {
  final routeName;

  static final navigatorKey = new GlobalKey<NavigatorState>();

  NavigateMiddleware({
    this.routeName,
  });

//  List<Middleware<AppState>> navigateMiddleware() {
//
//    final navigatorKey = NoomiKeys.navKey;
//
//    final navigatePushNamed = _navigatePushNamed(navigatorKey);
//    return ([
//      TypedMiddleware<AppState, NavigateAction>(navigatePushNamed),
//    ]);
//  }

  Middleware<AppState> _navigatePushNamed(navigatorKey) {
    return (Store<AppState> store, action, NextDispatcher next) {
      next(action);
      navigatorKey.currentState.pushNamed(action.to);
    };
  }

  @override
  void call(Store<AppState> store, action, NextDispatcher next) {
    // TODO: implement call
    next(action);

    if (action is NavigateLoginPageAction) {
      navigatorKey.currentState.pushNamed("/LoginPage");
    }

    if (action is NavigateOtpAction) {
      navigatorKey.currentState.pushNamed("/otp");
    }

    if (action is NavigateRegisterAction) {
      navigatorKey.currentState.pushNamed("/register");
    }

    if (action is LoadingActionScreen) {
      navigatorKey.currentState.pushNamed("/LoadingScreen");
    }

    if (action is SuperAdminScreen) {
      navigatorKey.currentState.pushNamed("/SuAdminScreen");
    }

    if (action is TeacherHomeAppScreen) {
      navigatorKey.currentState.pushNamed("/TeacherScreen");
    }

    if (action is paidStudentScreen) {
      navigatorKey.currentState.pushNamed("/pStudentScreen");
    }

    if (action is normalStudentScreen) {
      navigatorKey.currentState.pushNamed("/nStudentScreen");
    }

    if (action is prefrenceScreen) {
      navigatorKey.currentState.pushNamed("/prefrenceScreen");
    }

    if (action is advScreen) {
      navigatorKey.currentState.pushNamed("/AddVedScreen");
    }

    if (action is anlyScreen) {
      navigatorKey.currentState.pushNamed("/AnalyticScreen");
    }

    if (action is UserManagementScreen) {
      navigatorKey.currentState.pushNamed("/UserManageScreen");
    }

    if (action is CScreen) {
      navigatorKey.currentState.pushNamed("/CategoryScreen");
    }

    if (action is AuthorScreen1) {
      navigatorKey.currentState.pushNamed("/AuthorScreen");
    }

    if (action is CourseScreen1) {
      navigatorKey.currentState.pushNamed("/CourseScreen");
    }

    if (action is SettingsScreen) {
      navigatorKey.currentState.pushNamed("/SettingsScreen");
    }

    if (action is profileScreen1) {
      navigatorKey.currentState.pushNamed("/profileScreen1");
    }

    if (action is authorprofile) {
      navigatorKey.currentState.pushNamed("/authorprofile");
    }

    if (action is uiProfile1) {
      navigatorKey.currentState.pushNamed("/uiProfile");
    }

    if (action is vedioList) {
      navigatorKey.currentState.pushNamed("/vedioList");
    }

    if (action is refreshScreen1) {
      navigatorKey.currentState.pushNamed("/refreshScreen");
    }

    if (action is errorScreen1) {
      navigatorKey.currentState.pushNamed("/errorScreen");
    }

    if (action is Categoryprofile1) {
      navigatorKey.currentState.pushNamed("/Categoryprofile");
    }
  }
}

class NavigateLoginPageAction {}

class NavigateOtpAction {}

class NavigateRegisterAction {}

class LoadingActionScreen {}

class normalStudentScreen {}

class TeacherHomeAppScreen {}

class SuperAdminScreen {}

class paidStudentScreen {}

class prefrenceScreen {}

class advScreen {}

class anlyScreen {}

class UserManagementScreen {}

//SuperAdmin Screen

class CScreen {}

class AuthorScreen1 {}

class CourseScreen1 {}

class vedioList {}

class refreshScreen1 {}

class errorScreen1 {}

//user SCreen
class SettingsScreen {}

class profileScreen1 {}

class authorprofile {}

class Categoryprofile1 {}

class uiProfile1 {
  final String name;
  final String discription;
  final String Qual;

  uiProfile1({this.name, this.discription, this.Qual});
}

abstract class ApiStrategy {
  bool canHandle(dynamic action);
  bool handle(Store<AppState> store, dynamic action, String authorization);
}

class ApiMiddleware implements MiddlewareClass<AppState> {
  final List<ApiStrategy> strategies;

  ApiMiddleware({this.strategies});

  DateTime lastRefresh = DateTime.now();

  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) {
    if (store.state.firebaseUser != null) {
      bool refreshToken = false;
      DateTime now = DateTime.now();
      if (now.difference(lastRefresh).inMinutes > 5) {
        lastRefresh = now;
        refreshToken = true;
      }
      store.state.firebaseUser.getIdToken(refresh: refreshToken).then((value) {
        String authorization = "Bearer " + value;
        for (var strategy in this.strategies) {
          if (strategy.canHandle(action)) {
            strategy.handle(store, action, authorization);
          }
        }
      }).catchError((e, s) {
        debugPrint("get id token error: " + e.toString());
        store.dispatch(Unlock());
      });
    }

    // Make sure to forward actions to the next middleware in the chain!
    next(action);
  }
}
