part of app.widgets;

//class paidStudent extends StatefulWidget {
//  @override
//  _paidStudentState createState() => _paidStudentState();
//}
//
//class _paidStudentState extends State<paidStudent> {
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold();
//  }
//}

class paidStudent extends StatefulWidget {
  paidStudent({
    Key key,
  }) : super(key: key);

  @override
  _paidStudentState createState() => _paidStudentState();
}

class _paidStudentState extends State<paidStudent> {
  final _formKey = GlobalKey<FormState>();
  String _email, _password;
  bool _isObscured = true;
  Color _eyeButtonColor = Colors.grey;
  Future<String> _message = Future<String>.value('');




  @override
  Widget build(BuildContext context) {
    return Container(
        child:  new StoreConnector<AppState, AddUserModel>(
          converter: (store) {
            return AddUserModel(
              addUserState: store.state.addUserState,
              loadingState: store.state.loadingState,
              user: store.state.user,
            );
          },
          builder: (BuildContext context, AddUserModel au) {
            var name = au.user.UserName.text;

            var User = au.user;

//          User.uiid.text ;
//          User.UserName.text ;
//          User.gender.text ;
//          User.email.text ;
//          User.phone.text ;
//          User.address.text ;
//          User.city.text ;
//          User.DOB.text ;
//          User.state.text ;
//          User.age.text ;
//          User.date_register.text ;
//          User.imageUri.text ;
//          User.Occupation.text ;
//          // EduCareId: datasnapshot.data['EduCareId'],
//          User.EduCareId.text ;
            Padding buildTitle(String txt) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  txt,
                  style: TextStyle(fontSize: 42.0),
                ),
              );
            }

            Padding buildTitleLine() {
              return Padding(
                padding: const EdgeInsets.only(top: 4.0, left: 12.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    width: 38.0,
                    height: 1.5,
                    color: Colors.black,
                  ),
                ),
              );
            }

            return new Scaffold(

              appBar: AppBar(title: Text("PaidStudent  Screen"),),
              body: Container(
                child: Column(
                  children: <Widget>[
                    buildTitle('Welcome To the Paid Student'),
                    buildTitleLine(),

                    CardSettings(
                      contentAlign: TextAlign.start,
                      labelAlign: TextAlign.start,
                      children: <Widget>[
                        Text("Name :" + User.UserName.text),
                        Text("gender :" + User.gender.text),
                        Text("email :" + User.email.text),
                        Text("phone :" + User.phone.text),
                        Text("address :" + User.address.text),
                        Text("city :" + User.city.text),


                        Text("DOB :" + User.DOB.text),
                        Text("state :" + User.state.text),
                        Text("age :" + User.age.text),
                        Text("date_register :" + User.date_register.text),
                        Text("imageUri :" + User.imageUri.text),
                        Text("Occupation :" + User.Occupation.text),
                      ],
                    ),
                  ],
                ),
              ),

            );
          },
        ));
  }
}