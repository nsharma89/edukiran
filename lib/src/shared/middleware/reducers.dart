part of app.middlewares;

final middlewareReducers = combineReducers<AppState>([
  TypedReducer<AppState, LogInSuccessful>(_logIn),
  TypedReducer<AppState, LogOutSuccessful>(_logOut),
]);
