import 'package:edu_kiran/src/shared/middleware/middlewares.dart';
import 'package:edu_kiran/src/shared/widgets/widgets.dart';
import 'package:edu_kiran/src/shared/state/appState.dart';
import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux_logging/redux_logging.dart';

void main() {
  Store<AppState> store = new Store<AppState>(
      combineReducers<AppState>([stateReducers, middlewareReducers]),
      initialState: AppState.initial(),
      middleware: [
        new LoggingMiddleware.printer(),
        new LoginMiddleware(),
        new LogoutMiddleware(),
        new NavigateMiddleware(),
        new dataMiddleware(),
      ]);

  runApp(new HomeScreen(store: store));
}

class HomeScreen extends StatelessWidget {
  final Store<AppState> store;

  HomeScreen({Key key, this.store}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new StoreProvider<AppState>(
      store: store,
      child: new LoginOrHome(),
    );
  }
}

class LoginOrHome extends StatelessWidget {
  final Store<AppState> store;
  LoginOrHome({Key key, this.store}) : super(key: key);

//  void initState() async {
//    var db = new DatabaseHelper();
//    var isLoggedIn = await db.isLoggedIn();
//    if(isLoggedIn)
//      store.dispatch( new fetchUserData());
//
//  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        theme: ThemeData(
          accentColor: Colors.indigo[400], // used for card headers

          backgroundColor: Colors.black87, // color outside the card
          primaryColor: Colors.deepPurple, // color of page header
          buttonColor:
              Colors.lightBlueAccent[100], //// background color of buttons
          inputDecorationTheme: InputDecorationTheme(
            fillColor: Colors.lightBlueAccent[100],
            border: OutlineInputBorder(
                borderSide: BorderSide(
              style: BorderStyle.solid,
            )),
            filled: false,
          ),
          textTheme: TextTheme(
            button: TextStyle(
                color: Colors.deepPurple[900]), // style of button text
            subhead: TextStyle(color: Colors.black), // style of input text
          ),
        ),
        routes: <String, WidgetBuilder>{
          '/LoginPage': (BuildContext context) => new LoginPage(),
          '/otp': (BuildContext context) => new OtpPage(),
          '/register': (BuildContext context) => new RegisterPage(),
          //'/LoadingScreen': (BuildContext context) => new LoadingScreen (),
          '/SuAdminScreen': (BuildContext context) => new SuperAdmin(),
          '/TeacherScreen': (BuildContext context) => new TeacherHomeApp(),
          '/pStudentScreen': (BuildContext context) => new paidStudent(),
          '/nStudentScreen': (BuildContext context) => new normalStudent(),

          '/prefrenceScreen': (BuildContext context) => new prefrencePage(),

          '/AddVedScreen': (BuildContext context) => new AddVedScreen(),
          '/UserManageScreen': (BuildContext context) => new UserManageScreen(),
          '/AnalyticScreen': (BuildContext context) => new AnalyticScreen(),

          //superAdminPages
          '/CategoryScreen': (BuildContext context) => new CategoryScreen(),
          '/AuthorScreen': (BuildContext context) => new AuthorScreen(),
          '/CourseScreen': (BuildContext context) => new CourseScreen(),

          //user's screenprofileScreen1
          '/profileScreen1': (BuildContext context) => new AddProfileScreen(),
          '/SettingsScreen': (BuildContext context) => new Setting(),
          '/authorprofile': (BuildContext context) => new Authorprofile(),

          '/uiProfile': (BuildContext context) => new uiProfile(),
          '/vedioList': (BuildContext context) => new AddedVideoScreen(),
          '/refreshScreen': (BuildContext context) => new refreshScreen(),
          '/errorScreen': (BuildContext context) => new errorScreen(),
          '/Categoryprofile': (BuildContext context) => new Categoryprofile(),
        },
        navigatorKey: NavigateMiddleware.navigatorKey,
        home: new StoreConnector<AppState, bool>(converter: (store) {
          return store.state.firebaseUser != null;
        }, builder: (BuildContext context, bool isLogged) {
//      if (!isLogged) {
//        return new LoginPage();
//      } else {LoadingScreen
//        return new MyApp();
//      }MyApp  AddVedScreen pPlayer SuperAdmin

          return new pPlayer();
        }));
  }
}
