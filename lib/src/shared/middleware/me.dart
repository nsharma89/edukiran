part of app.middlewares;

//class ApiMeAction {}
//class ApiMeResponseAction {
//  final User data;
//  final ApiException error;
//  ApiMeResponseAction(this.data,this.error);
//}
//
//class MeStrategy extends ApiStrategy {
//  final MeApi api;
//  MeStrategy({this.api});
//  factory MeStrategy.New() {
//    return new MeStrategy(
//      api: new MeApi(),
//    );
//  }
//
//  bool canHandle(action) {
//    return action is ApiMeAction;
//  }
//
//  bool handle(Store<AppState> store, dynamic action, String authorization) {
//    this.api.me(authorization).then((result) {
//      debugPrint(result.toString());
//      store.dispatch(new ApiMeResponseAction(result,null));
//    }).catchError((e, s)  {
//      debugPrint(e.toString());
//      debugPrint(s.toString());
//      store.dispatch(new ApiMeResponseAction(null,e));
//    });
//  }
//}
//
//AppState _meResponse(AppState state, ApiMeResponseAction action) {
//  debugPrint("me response: " + action.data.toString());
//  return state.copyWith(loadingState: state.loadingState.copyWith(meState: MeState.initialFromUser(action.data, action.error)));
//}