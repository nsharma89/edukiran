part of app.state;

class AddYoutubeVedio {
  // Registration response
  final bool registrationError;
  final String registrationMessage;
  final bool registrationOnGoing;


  final String filePath;


  final List<String> tags;

  final TextEditingController TitleController;
  final TextEditingController descriptionController;
  final TextEditingController tagsController;
  final String categoryIdController;



  AddYoutubeVedio({
    this.registrationOnGoing,
    this.registrationMessage,
    this.registrationError,
    this.filePath,
    this.TitleController,
    this.descriptionController,
    this.categoryIdController,
    this.tags,
    this.tagsController
  });

  factory AddYoutubeVedio.initial() {
    return new AddYoutubeVedio(
      TitleController: new TextEditingController(),
      descriptionController: new TextEditingController(),
      tagsController: new TextEditingController(),
      categoryIdController: "27",
      tags: [],
    );
  }

  AddYoutubeVedio copyWith({
    bool registrationError,
    String registrationMessage,
    bool registrationOnGoing,

    String filePath,
    List<String> tags,
  }) {
    return new AddYoutubeVedio(
      TitleController: this.TitleController,
      descriptionController: this.descriptionController,
      tagsController: this.tagsController,
      categoryIdController: "27",
      registrationOnGoing: registrationOnGoing ?? this.registrationOnGoing,
      registrationError: registrationError ?? this.registrationError,
      registrationMessage: registrationMessage ?? this.registrationMessage,
      filePath: filePath != null && filePath != "" ? filePath : this.filePath,
      tags: tags != null && tags != "" ? tags : this.tags,
    );
  }
}

final addvedioreducer = combineReducers<AppState>([
  TypedReducer<AppState, addvedioPath>(_addvedioPath),
]);


class addvedioPath {
  final String value;
  addvedioPath(this.value);
}

AppState _addvedioPath(
    AppState state, addvedioPath action) {
  return state.copyWith(
      addYoutubeVedio: state.addYoutubeVedio.copyWith(filePath: action.value));
}