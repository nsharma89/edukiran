part of app.widgets;

class AddedVideoScreen extends StatefulWidget {
  AddedVideoScreen({
    Key key,
  }) : super(key: key);

  @override
  _AddedVideoScreenState createState() => _AddedVideoScreenState();
}

class _AddedVideoScreenState extends State<AddedVideoScreen> {



  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, AddAuthorModel>(
      converter: (store) {
        return AddAuthorModel(
          addAuthor: store.state.addAuthor,
          onSave: (){

          },
          navigate: (s) {
            if(s == "addVedio"){
              store.dispatch(advScreen());
            }
            if(s == "usm"){
              store.dispatch(UserManagementScreen());
            }
            if(s == "aly"){
              store.dispatch(anlyScreen());
            }

            if(s == "CScree"){
              store.dispatch(CScreen());
            }
            if(s == "Author"){
              store.dispatch(AuthorScreen1());
            }
            if(s == "Course"){
              store.dispatch(CourseScreen1());
            }

            if(s == "set"){
              store.dispatch(SettingsScreen());
            }
            if(s == "prof"){
              store.dispatch(profileScreen1());
            }


          },
        );
      },
      builder: (BuildContext context, AddAuthorModel au) {
        return Scaffold(
          body: CustomScrollView(
            primary: false,
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 150.0,
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0 ,right:8.0 ),
                    child: IconButton(
                      icon: Icon(Icons.perm_identity),
                      onPressed: () => au.navigate("prof"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0,right:8.0 ),
                    child: IconButton(
                      icon: Icon(Icons.settings),
                      onPressed: () => au.navigate("set"),
                    ),
                  ),

                ],
                backgroundColor: Colors.black45,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text('Video List'),
                  background: Image.asset("asset/fg2.jpg" ,fit: BoxFit.fitWidth,height: 150.0,),
                  collapseMode: CollapseMode.parallax,
                ),
              ),

              SliverPadding(
                  padding: const EdgeInsets.only(left: 5.0,right: 5.0,bottom: 20.0,top: 20.0),
                  sliver: SliverList(
                      delegate: SliverChildBuilderDelegate((builder , index){
                        return Container(
                          height: 500.0,
                          child: StreamBuilder(
                            stream: Firestore.instance.collection('videoData').snapshots(),
                            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (!snapshot.hasData) return CircularProgressIndicator();
                              return listSection(documents: snapshot.data.documents,au: au,);
                            },
                          ),
                        );
                      },
                        childCount: 1,
                      ))
              ),

            ],
          ),

        );
      },
    );;
  }
}

class listSection extends StatelessWidget {
  final List<DocumentSnapshot> documents;
  final AddAuthorModel au;

  listSection({this.documents,this.au});

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: documents.length,
      itemExtent: 110.0,
      itemBuilder: (BuildContext context, int index) {
        String title = documents[index].data['Name'].toString();
        String score = documents[index].data['url'].toString();
        String details = documents[index].data['Description'].toString();
        return Container(
          decoration: BoxDecoration(border: Border.all(color: Colors.deepPurple,width: 1.0),),

          child: Center(
            child: ListTile(
              leading: Icon(Icons.vertical_align_bottom,color: Colors.green,size: 40.0,),          subtitle: Text("URL : $score"),

              title: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(color: Colors.white),
                ),
                padding: EdgeInsets.all(5.0),
                child: Text("Name : $title"),

              ),
              onTap: (){
                _launchURL(score);
              },
            ),
          ),
        );
      },
    );
  }
}