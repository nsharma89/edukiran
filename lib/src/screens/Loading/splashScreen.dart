part of app.widgets;

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool value = true;

  void dbCall() async {
    var db = new DatabaseHelper();
    bool a = await db.isLoggedIn();
    if(a){
      value = false;
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dbCall();
  }

  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 10,
        navigateAfterSeconds:  value  ? new SuperAdmin() : fetchUserData(),
        title: new Text('Welcome In EduTuts',
          style: new TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 24.0
          ),),
        image: new Image.asset("asset/logo.png"),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 170.0,
        onClick: ()=>print("Flutter Egypt"),
        loaderColor: Colors.teal,
    );
  }
}

