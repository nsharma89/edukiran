part of app.widgets;

class Authorprofile extends StatefulWidget {
  Authorprofile({
    Key key,
  }) : super(key: key);

  @override
  _AuthorprofileState createState() => _AuthorprofileState();
}

class _AuthorprofileState extends State<Authorprofile> {
  String _filePath;

  void getFilePath(AddAuthorModel au) async {
    try {
      File filePath = await ImagePicker.pickVideo(source: ImageSource.gallery);
      if (filePath == '') {
        return;
      }

      print("File path: " + filePath.path);
      setState((){this._filePath = filePath.path;
      au.anything1();
      });
    } on PlatformException catch (e) {
      print("Error while picking the file: " + e.toString());
    }
  }

  Future<List<String>> _getData() async {
    var values = new List<String>();
    values.add("Horses");
    values.add("Goats");
    values.add("Chickens");

    //throw new Exception("Danger Will Robinson!!!");

    await new Future.delayed(new Duration(seconds: 5));

    return values;
  }

  Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    List<String> values = snapshot.data;
    return new ListView.builder(
      itemCount: values.length,
      itemBuilder: (BuildContext context, int index) {
        return new Column(
          children: <Widget>[
            new ListTile(
              title: new Text(values[index]),
            ),
            new Divider(height: 2.0,),
          ],
        );
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, AddAuthorModel>(
      converter: (store) {
        return AddAuthorModel(
          addAuthor: store.state.addAuthor,
          onSave: (){

          },
          anything1: (){
            store.dispatch(addImagePath(_filePath));
            print("ok");
          },
          navigate: (s) {
            if(s == "addVedio"){
              store.dispatch(advScreen());
            }
            if(s == "usm"){
              store.dispatch(UserManagementScreen());
            }
            if(s == "aly"){
              store.dispatch(anlyScreen());
            }

            if(s == "CScree"){
              store.dispatch(CScreen());
            }
            if(s == "Author"){
              store.dispatch(AuthorScreen1());
            }
            if(s == "Course"){
              store.dispatch(CourseScreen1());
            }

            if(s == "set"){
              store.dispatch(SettingsScreen());
            }
            if(s == "prof"){
              store.dispatch(profileScreen1());
            }


          },
          showProfile: (name,dis,qal){
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => uiProfile(name: name , discription: dis, Qual: qal)),
            );
//            store.dispatch(uiProfile1(name: name , discription: dis, Qual: qal));
          }
        );
      },
      builder: (BuildContext context, AddAuthorModel au) {
        return Scaffold(
          body: CustomScrollView(
            primary: false,
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 150.0,
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0 ,right:8.0 ),
                    child: IconButton(
                      icon: Icon(Icons.perm_identity),
                      onPressed: () => au.navigate("prof"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0,right:8.0 ),
                    child: IconButton(
                      icon: Icon(Icons.settings),
                      onPressed: () => au.navigate("set"),
                    ),
                  ),

                ],
                backgroundColor: Colors.black45,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text('Our Author'),
                  background: Image.asset("asset/fg3.jpg" ,fit: BoxFit.fitWidth,height: 150.0,),
                  collapseMode: CollapseMode.parallax,
                ),
              ),

              SliverPadding(
                  padding: const EdgeInsets.only(left: 5.0,right: 5.0,bottom: 20.0,top: 20.0),
                  sliver: SliverList(
                      delegate: SliverChildBuilderDelegate((builder , index){
                        return Container(
                          height: 500.0,
                          child: StreamBuilder(
                            stream: Firestore.instance.collection('authorData').snapshots(),
                            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (!snapshot.hasData) return CircularProgressIndicator();
                              return FirestoreListView(documents: snapshot.data.documents,au: au,);
                            },
                          ),
                        );
                      },
                        childCount: 1,
                      ))
              ),

            ],
          ),

        );
      },
    );;
  }
}


class FirestoreListView extends StatelessWidget {
  final List<DocumentSnapshot> documents;
  final AddAuthorModel au;

  FirestoreListView({this.documents,this.au});

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: documents.length,
      itemExtent: 110.0,
      addRepaintBoundaries: true,
      itemBuilder: (BuildContext context, int index) {
        String title = documents[index].data['Name'].toString();
        String score = documents[index].data['Qualificaton'].toString();
        String details = documents[index].data['Description'].toString();
        return Container(
          decoration: BoxDecoration(border: Border.all(color: Colors.deepPurple,width: 1.0),),
          child: Center(
            child: ListTile(

              leading: Icon(Icons.vertical_align_bottom,color: Colors.green,size: 40.0,),
                subtitle: Text("Description : $score"),

                title: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(color: Colors.white),
                  ),
                  padding: EdgeInsets.all(5.0),
                 child: Text("Name : $title"),

//              child: Row(
//                children: <Widget>[
//                  Expanded(
//                    child: !documents[index].data['editing']
//                        ? Text(title)
//                        : TextFormField(
//                      initialValue: title,
//                      onFieldSubmitted: (String item) {
//                        Firestore.instance
//                            .runTransaction((transaction) async {
//                          DocumentSnapshot snapshot = await transaction
//                              .get(documents[index].reference);
//
//                          await transaction.update(
//                              snapshot.reference, {'title': item});
//
//                          await transaction.update(snapshot.reference,
//                              {"editing": !snapshot['editing']});
//                        });
//                      },
//                    ),
//                  ),
//                  Text("$score"),
////                  Column(
////                    children: <Widget>[
////                      IconButton(
////                        onPressed: () {
////                          Firestore.instance
////                              .runTransaction((Transaction transaction) async {
////                            DocumentSnapshot snapshot = await transaction
////                                .get(documents[index].reference);
////                            await transaction.update(snapshot.reference,
////                                {'score': snapshot['score'] + 1});
////                          });
////                        },
////                        icon: Icon(Icons.arrow_upward),
////                      ),
////                      IconButton(
////                        onPressed: () {
////                          Firestore.instance
////                              .runTransaction((Transaction transaction) async {
////                            DocumentSnapshot snapshot = await transaction
////                                .get(documents[index].reference);
////                            await transaction.update(snapshot.reference,
////                                {'score': snapshot['score'] - 1});
////                          });
////                        },
////                        icon: Icon(Icons.arrow_downward),
////                      ),
////                    ],
////                  ),
////                  IconButton(
////                    icon: Icon(Icons.delete),
////                    onPressed: () {
////                      Firestore.instance.runTransaction((transaction) async {
////                        DocumentSnapshot snapshot =
////                        await transaction.get(documents[index].reference);
////                        await transaction.delete(snapshot.reference);
////                      });
////                    },
////                  )
//                ],
//              ),
                ),
                onTap: (){
                  au.showProfile(title,details,score);
                },
            ),
          ),
        );
      },
    );
  }
}