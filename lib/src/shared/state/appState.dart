library app.state;

import 'package:flutter/foundation.dart';
import 'package:redux/redux.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'dart:io';

import 'dart:async';

part 'package:edu_kiran/src/shared/state/state.dart';
part 'package:edu_kiran/src/screens/Loading/state.dart';
part 'user.dart';
part 'package:edu_kiran/src/screens/Signup/state.dart';
part 'package:edu_kiran/src/shared/state/addvediostate.dart';
part 'package:edu_kiran/src/shared/state/addCategoryState.dart';
part 'package:edu_kiran/src/shared/state/addCourseState.dart';
part 'package:edu_kiran/src/shared/state/authorState.dart';
