part of app.widgets;

class AnalyticScreen extends StatefulWidget {
  AnalyticScreen({
    Key key,
  }) : super(key: key);

  @override
  _AnalyticScreenState createState() => _AnalyticScreenState();
}

class _AnalyticScreenState extends State<AnalyticScreen> {





  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, AddUserModel>(
      converter: (store) {
        return AddUserModel(
            addUserState: store.state.addUserState,
            loadingState: store.state.loadingState,
            onMobilecode: (s){
              store.dispatch(CodeDropdownChange(s));
            }
        );
      },
      builder: (BuildContext context, AddUserModel au) {
        return Scaffold(
          appBar: AppBar(
            title: Text("Analytic Screen"),
          ),
        );
      },
    );;
  }
}