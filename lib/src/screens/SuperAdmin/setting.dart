part of app.widgets;

class Setting extends StatefulWidget {
  Setting({
    Key key,
  }) : super(key: key);

  @override
  _SettingScreenState createState() => _SettingScreenState();
}

class _SettingScreenState extends State<Setting> {

  Widget addtextfield(String LabelText, IconData IconS ,TextEditingController controller ,{Function validator}){
    return Padding(
      padding: const EdgeInsets.only(left: 4.0,right: 4.0),
      child: TextFormField(

        decoration: InputDecoration(
          labelText: LabelText,
          prefixIcon: Icon(IconS),
        ),
        controller: controller,
        keyboardType: TextInputType.text,
        validator: validator ?? null,
      ),
    );
  }



  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, AddCategoryModel>(
      converter: (store) {
        return AddCategoryModel(
            addCategoryVedio: store.state.addCategoryVedio,
            onOccupationStudent: (s){
              store.dispatch(CodeDropdownChange(s));
            },
            onSave: (){

            }
        );
      },
      builder: (BuildContext context, AddCategoryModel au) {
        return Scaffold(
          body: CustomScrollView(
            primary: false,
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 150.0,
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0 ,right:8.0 ),
                    child: Icon(Icons.perm_identity),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0,right:8.0 ),
                    child: Icon(Icons.settings),
                  ),

                ],
                backgroundColor: Colors.black45,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text('Settings'),
                  background: Image.asset("asset/fg4.png" ,fit: BoxFit.fitWidth,height: 150.0,),
                  collapseMode: CollapseMode.parallax,
                ),
              ),

              SliverPadding(
                  padding: const EdgeInsets.only(left: 5.0,right: 5.0,bottom: 20.0,top: 20.0),
                  sliver: SliverList(
                      delegate: SliverChildBuilderDelegate((builder , index){
                        return CardSettings(
                            children: <Widget>[
                              Container(
                                height: 200.0,
                                width: 200.0,
                                color: Colors.teal,
                                child: Center(
                                  child: Text("To Be implemented:"),
                                ),
                              )
                            ],

                        );
                      },
                        childCount: 1,
                      ))
              ),

            ],
          ),

        );
      },
    );;
  }
}


class AddSettingModel {
  final AddCategoryVedio addCategoryVedio;

  final void Function(dynamic s) onOccupationStudent;
  final void Function(dynamic s) onOccupationTEacher;
  final void Function(dynamic s) onGenderChange;
  final _VerifyeViewModel VerifyeViewModel;
  final AppState appState;


  final void Function() onSave;


  AddSettingModel({
    this.addCategoryVedio,
    this.onOccupationStudent,
    this.onOccupationTEacher,
    this.onSave,
    this.VerifyeViewModel,
    this.appState,
    this.onGenderChange,
  });

}