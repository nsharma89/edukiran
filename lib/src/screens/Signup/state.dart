part of app.state;

class AddUserState {
  // Registration response
  final bool registrationError;
  final String registrationMessage;
  final bool registrationOnGoing;

  final TextEditingController UserNameController;
  final String Occupation;
  final String MobileCode;
  final String Gender;

  final List<String> SubjectPrefrence;

  final TextEditingController passwordController;
  final TextEditingController emailController;
  final TextEditingController ageController;

  final TextEditingController AddressController;
  final TextEditingController DOBController;
  final TextEditingController GenderController;

  final TextEditingController StateController;
  final TextEditingController CityController;
  final TextEditingController PhoneNumController;
  final TextEditingController smsCodeController;

  // Form keys used for validation
  final GlobalKey<FormState> signUpNow;
  final GlobalKey<FormState> verify;

  AddUserState({
    this.registrationOnGoing,
    this.registrationMessage,
    this.registrationError,
    this.AddressController,
    this.ageController,
    this.CityController,
    this.DOBController,
    this.emailController,
    this.GenderController,
    this.Occupation,
    this.Gender,
    this.passwordController,
    this.PhoneNumController,
    this.signUpNow,
    this.StateController,
    this.UserNameController,
    this.verify,
    this.smsCodeController,
    this.MobileCode,
    this.SubjectPrefrence,
  });

  factory AddUserState.initial() {
    return new AddUserState(
      AddressController: new TextEditingController(),
      ageController: new TextEditingController(),
      CityController: new TextEditingController(),
      DOBController: new TextEditingController(),
      emailController: new TextEditingController(),
      GenderController: new TextEditingController(),
      Occupation: "",
      Gender: "Male",
      passwordController: new TextEditingController(),
      PhoneNumController: new TextEditingController(),
      registrationError: false,
      registrationMessage: "",
      registrationOnGoing: false,
      signUpNow: new GlobalKey<FormState>(debugLabel: "sign-up-now-key"),
      StateController: new TextEditingController(),
      UserNameController: new TextEditingController(),
      verify: new GlobalKey<FormState>(debugLabel: "verify-key"),
      smsCodeController: new TextEditingController(),
      MobileCode: "+91",
      SubjectPrefrence: [],
    );
  }

  AddUserState copyWith({
    bool registrationError,
    String registrationMessage,
    bool registrationOnGoing,
    String UserName,
    String phone,
    String password,
    String email,
    String age,
    String Occupation,
    String MobileCode,
    String Address,
    String DOB,
    String Gender,
    String State,
    String City,
    String PhoneNum,
    List<String> SubjectPrefrence,
  }) {
    return new AddUserState(
      AddressController: this.AddressController,
      ageController: this.ageController,
      CityController: this.CityController,
      DOBController: this.DOBController,
      emailController: this.emailController,
      GenderController: this.GenderController,
      passwordController: this.passwordController,
      PhoneNumController: new TextEditingController(),
      registrationOnGoing: registrationOnGoing ?? this.registrationOnGoing,
      registrationError: registrationError ?? this.registrationError,
      registrationMessage: registrationMessage ?? this.registrationMessage,
      StateController: new TextEditingController(),
      UserNameController: new TextEditingController(),
      smsCodeController: this.smsCodeController,
      Occupation:
          Occupation != null && Occupation != "" ? Occupation : this.Occupation,
      Gender: Gender != null && Gender != "" ? Gender : this.Gender,
      MobileCode:
          MobileCode != null && MobileCode != "" ? MobileCode : this.MobileCode,
      verify: this.verify,
      SubjectPrefrence: SubjectPrefrence != null && SubjectPrefrence != ""
          ? SubjectPrefrence
          : this.SubjectPrefrence,
    );
  }
}

class OccupationDropdownChange {
  final String value;
  OccupationDropdownChange(this.value);
}

class GenderDropdownChange {
  final String value;
  GenderDropdownChange(this.value);
}

final formStateReducer = combineReducers<AppState>([
  TypedReducer<AppState, OccupationDropdownChange>(_OccupationDropdownChange),
  TypedReducer<AppState, GenderDropdownChange>(_GenderDropdownChange),
  TypedReducer<AppState, ResetAddUserState>(_resetAddKidForm),
  TypedReducer<AppState, CodeDropdownChange>(_CodeDropdownChange),
  TypedReducer<AppState, AddingSubjectPre>(_AddingSubjectPre),
]);

AppState _OccupationDropdownChange(
    AppState state, OccupationDropdownChange action) {
  return state.copyWith(
      addUserState: state.addUserState.copyWith(Occupation: action.value));
}

AppState _GenderDropdownChange(AppState state, GenderDropdownChange action) {
  return state.copyWith(
      addUserState: state.addUserState.copyWith(Gender: action.value));
}

class CodeDropdownChange {
  final String value;
  CodeDropdownChange(this.value);
}

AppState _CodeDropdownChange(AppState state, CodeDropdownChange action) {
  return state.copyWith(
      addUserState: state.addUserState.copyWith(MobileCode: action.value));
}

class AddingSubjectPre {
  final List<String> value;
  AddingSubjectPre(this.value);
}

AppState _AddingSubjectPre(AppState state, AddingSubjectPre action) {
  return state.copyWith(
      addUserState:
          state.addUserState.copyWith(SubjectPrefrence: action.value));
}

class Lock {}

class Unlock {}

AppState _unlock(AppState state, Unlock action) {
  return state.copyWith(disableUserInputAndShowLoadingAnimation: false);
}

AppState _lock(AppState state, Lock action) {
  return state.copyWith(disableUserInputAndShowLoadingAnimation: true);
}

final lockUnlockReducers = combineReducers<AppState>([
  TypedReducer<AppState, Unlock>(_unlock),
  TypedReducer<AppState, Lock>(_lock)
]);

//reset
class ResetAddUserState {}

AppState _resetAddKidForm(AppState state, ResetAddUserState action) {
  return state.copyWith(addUserState: AddUserState.initial());
}
