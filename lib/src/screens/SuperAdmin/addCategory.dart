part of app.widgets;

class CategoryScreen extends StatefulWidget {
  CategoryScreen({
    Key key,
  }) : super(key: key);

  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  static GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static GlobalKey<FormState> _formKey2 = GlobalKey<FormState>();

  static GlobalKey<ScaffoldState> _ScaffoldStatekey =
      GlobalKey<ScaffoldState>();

  Widget addtextfield(
      String LabelText, IconData IconS, TextEditingController controller,
      {Function validator, int Maxlines}) {
    return Padding(
      padding: const EdgeInsets.only(left: 4.0, right: 4.0),
      child: TextFormField(
        decoration: InputDecoration(
          labelText: LabelText,
          prefixIcon: Icon(IconS),
        ),
        controller: controller,
        keyboardType: TextInputType.text,
        validator: validator,
        maxLines: Maxlines,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new StoreConnector<AppState, AddCategoryModel>(
      converter: (store) {
        return AddCategoryModel(
          addCategoryVedio: store.state.addCategoryVedio,
          onOccupationStudent: (s) {
            store.dispatch(CodeDropdownChange(s));
          },
          onSave: () {
            if (_formKey.currentState.validate()) {
              store.dispatch(refreshScreen1());
              store.dispatch(CategoryAdded());
//                store.dispatch(AuthorAdded());
            } else {
              _ScaffoldStatekey.currentState.showSnackBar(
                  TutSnackbar.error(text: "Please Enter the Values to field"));
            }
          },
          onSave2: () {
            if (_formKey2.currentState.validate()) {
              store.dispatch(refreshScreen1());
              store.dispatch(SubCategoryAdded());
//                store.dispatch(AuthorAdded());
            } else {
              _ScaffoldStatekey.currentState.showSnackBar(
                  TutSnackbar.error(text: "Please Enter the Values to field"));
            }
          },
          getClassesName: () {
            List<String> categoryName = new List<String>();
            categoryName.add("Select one");
            var c = Firestore.instance
                .collection('CategoryData')
                .getDocuments()
                .then((value) {
              for (var i in value.documents) {}
              for (var i = 0; i < 10; i++) {
                categoryName
                    .add(value.documents[i].data['CourseName'].toString());
              }
            });
            return categoryName;
          },
          onCategoryDropdownChange: (s) => store.dispatch(assignSubCategory(s)),
          navigate: (s) {
            if (s == "addVedio") {
              store.dispatch(advScreen());
            }
            if (s == "usm") {
              store.dispatch(UserManagementScreen());
            }
            if (s == "aly") {
              store.dispatch(anlyScreen());
            }

            if (s == "CScree") {
              store.dispatch(CScreen());
            }
            if (s == "Author") {
              store.dispatch(AuthorScreen1());
            }
            if (s == "Course") {
              store.dispatch(CourseScreen1());
            }

            if (s == "set") {
              store.dispatch(SettingsScreen());
            }
            if (s == "prof") {
              store.dispatch(profileScreen1());
            }
          },
        );
      },
      builder: (BuildContext context, AddCategoryModel au) {
        return Scaffold(
          key: _ScaffoldStatekey,
          body: CustomScrollView(
            primary: false,
            scrollDirection: Axis.vertical,
            slivers: <Widget>[
              SliverAppBar(
                pinned: true,
                expandedHeight: 150.0,
                actions: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: IconButton(
                      icon: Icon(Icons.perm_identity),
                      onPressed: () => au.navigate("prof"),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: IconButton(
                      icon: Icon(Icons.settings),
                      onPressed: () => au.navigate("set"),
                    ),
                  ),
                ],
                backgroundColor: Colors.black45,
                flexibleSpace: FlexibleSpaceBar(
                  title: Text('Add Category'),
                  background: Image.asset(
                    "asset/fg1.jpg",
                    fit: BoxFit.fitWidth,
                    height: 150.0,
                  ),
                  collapseMode: CollapseMode.parallax,
                ),
              ),
              SliverPadding(
                  padding: const EdgeInsets.only(
                      left: 5.0, right: 5.0, bottom: 20.0, top: 20.0),
                  sliver: SliverList(
                      delegate: SliverChildBuilderDelegate(
                    (builder, index) {
                      return Form(
                        key: _formKey,
                        child: CardSettings(
                          labelAlign: TextAlign.left,
                          labelPadding: 0.0,
                          contentAlign: TextAlign.left,
                          children: <Widget>[
                            SizedBox(
                              height: 30.0,
                            ),
                            addtextfield("Category Name", Icons.account_circle,
                                au.addCategoryVedio.NameController,
                                validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter Author name ';
                              }
                            }),
                            SizedBox(
                              height: 10.0,
                            ),
                            addtextfield("Details", Icons.list,
                                au.addCategoryVedio.descriptionController,
                                validator: (value) {
                              if (value.isEmpty) {
                                return 'Please enter Author name ';
                              }
                            }, Maxlines: 3),
                            SizedBox(
                              height: 10.0,
                            ),
//                            CardSettingsMultiselect(
//                              options: ["English","Grammar","Algorithm",
//                              "Spanish","France","AlienVoice"
//                              ],
//                              label: "Tags",
//                              initialValues: ["English","Grammar","AlienVoice",
//                              ],
//                              onChanged: (v){},
//                              onSaved: (v){
//                              },
//
//                            ),
//                            SizedBox(
//                              height: 10.0,
//                            ),
//                            CardSettingsMultiselect(
//                              options: ["12th","English","Grammar",
//                              "CBSE","Chapter-2","Maths"
//                              ],
//                              label: "Sub Category",
//                              initialValues: ["12th","English","Chapter",
//                              ],
//                              onChanged: (v){},
//                              onSaved: (v){
//                              },
//
//                            ),

                            SizedBox(
                              height: 30.0,
                            ),

                            Align(
                              child: SizedBox(
                                height: 50.0,
                                width: 200.0,
                                child: FlatButton(
                                  onPressed: au.onSave,
                                  color: Colors.deepPurple,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(30.0)),
                                  child: Text(
                                    'Add Category',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 30.0,
                            ),
                            Form(
                                key: _formKey2,
                                child: Column(
                                  children: <Widget>[
                                    addtextfield(
                                      "Sub-Category Name",
                                      Icons.account_circle,
                                      au.addCategoryVedio.SubCategoryName,
                                      validator: (value) {
                                        if (value.isEmpty) {
                                          return 'Please enter Sub-Category name ';
                                        }
                                      },
                                    ),
                                    SizedBox(
                                      height: 30.0,
                                    ),
                                    DropDown.dropdown(
                                        "Select Category",
                                        au.getClassesName(),
                                        au.onCategoryDropdownChange,
                                        au.addCategoryVedio.CategoryName),
                                    SizedBox(
                                      height: 30.0,
                                    ),
                                    Align(
                                      child: SizedBox(
                                        height: 50.0,
                                        width: 200.0,
                                        child: FlatButton(
                                          onPressed: au.onSave2,
                                          color: Colors.deepPurple,
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(30.0)),
                                          child: Text(
                                            'Add Sub-Category',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 30.0,
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      );
                    },
                    childCount: 1,
                  ))),
            ],
          ),
        );
      },
    );
    ;
  }
}

class AddCategoryModel {
  final AddCategoryVedio addCategoryVedio;

  final void Function(dynamic s) onOccupationStudent;
  final void Function(dynamic s) onCategoryDropdownChange;

  final List<String> Function() getClassesName;
  final void Function(dynamic s) onOccupationTEacher;
  final void Function(dynamic s) navigate;
  final _VerifyeViewModel VerifyeViewModel;
  final AppState appState;

  final void Function() onSave;
  final void Function() onSave2;

  AddCategoryModel({
    this.addCategoryVedio,
    this.onOccupationStudent,
    this.onOccupationTEacher,
    this.onSave,
    this.VerifyeViewModel,
    this.appState,
    this.navigate,
    this.getClassesName,
    this.onCategoryDropdownChange,
    this.onSave2,
  });
}
