part of app.middlewares;

class dataMiddleware implements MiddlewareClass<AppState> {
  @override
  void call(Store<AppState> store, dynamic action, NextDispatcher next) async {
    if (action is AuthorAdded) {
      final FirebaseAuth _auth = FirebaseAuth.instance;
      try {
        var author = store.state.addAuthor;

        Firestore.instance.collection('authorData').document().setData({
          'Name': author.NameController.text,
          'Description': author.descriptionController.text,
          'Qualificaton': author.QualificationController.text,
          'imageUri': "imageUri",

//          'phone': store.state.addUserState.PhoneNumController.text,
//          'address': store.state.addUserState.AddressController.text,
//          'city':  store.state.addUserState.CityController.text,
//          'DOB': store.state.addUserState.DOBController.text,
//          'state': store.state.addUserState.StateController.text,
//          'age':   "age",
//          'date_register': "date_register",
//          'imageUri': "imageUri",
//          'Occupation': store.state.addUserState.Occupation,
        });
        store.dispatch(authorprofile());
      } catch (error) {
        print(error);
      }
    }

    if (action is CategoryAdded) {
      final FirebaseAuth _auth = FirebaseAuth.instance;
      try {
        var author = store.state.addCategoryVedio;

        Firestore.instance.collection('CategoryData').document().setData({
          'CourseName': author.NameController.text,
          'Description': author.descriptionController.text,
          'imageUri': "imageUri",

//          'phone': store.state.addUserState.PhoneNumController.text,
//          'address': store.state.addUserState.AddressController.text,
//          'city':  store.state.addUserState.CityController.text,
//          'DOB': store.state.addUserState.DOBController.text,
//          'state': store.state.addUserState.StateController.text,
//          'age':   "age",
//          'date_register': "date_register",
//          'imageUri': "imageUri",
//          'Occupation': store.state.addUserState.Occupation,
        });
        store.dispatch(Categoryprofile1());
      } catch (error) {
        store.dispatch(new errorScreen(error: error));
        print(error);
      }
    }

    if (action is SubCategoryAdded) {
      try {
        var author = store.state.addCategoryVedio;

        Firestore.instance.collection('SubCategoryData').document().setData({
          'Name': author.SubCategoryName.text,
          'Category': author.CategoryName,
        });
        store.dispatch(Categoryprofile1());
      } catch (error) {
        store.dispatch(new errorScreen(error: error));
        print(error);
      }
    }

    if (action is dataAddedFirebase) {
      try {} catch (error) {
        store.dispatch(new errorScreen(error: error));
        print(error);
      }
    }

//    if (action is CategoryAdded) {
//      try {
//        List<String> cd;
//
//        var c = Firestore.instance.collection('CategoryData').getDocuments().then((value){
//          List<String> cd;
//          for(var i in value.documents){}
//          for(var i = 0 ; i< 10 ;i++) {
//            cd.add(value.documents[i].data['Name'].toString());
//          }
//          });
//
//        store.dispatch(Categoryprofile1());
//      } catch (error) {
//        store.dispatch(new errorScreen(error: error));
//        print(error);
//      }
//    }

    next(action);
  }
}

class AuthorAdded {}

class CategoryAdded {}

class SubCategoryAdded {}

class dataAddedFirebase {}
