part of app.widgets;

class errorScreen extends StatefulWidget {
 final String error;
  errorScreen({
    Key key,
    this.error
  }) : super(key: key);

  @override
  _errorScreenState createState() => _errorScreenState();
}

class _errorScreenState extends State<errorScreen> {


  @override
  Widget build(BuildContext context) {
    String error = widget.error;
    return Container(
      color: Colors.white,
      child: Stack(
          fit: StackFit.expand,
          children: [
            Center(child: new Text("Error found: $error",style: TextStyle(fontSize: 20.0,color: Colors.red),textAlign: TextAlign.center,))
          ]),
    );
  }
}

