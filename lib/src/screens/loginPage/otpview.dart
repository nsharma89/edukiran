part of app.widgets;

// A
final otpformKey = GlobalKey<FormState>();
class OtpPage extends StatefulWidget {
  OtpPage({
    Key key,
  }) : super(key: key);

  @override
  _OTpPageState createState() => _OTpPageState();
}

class _OTpPageState extends State<OtpPage> {
  final otpformKey = GlobalKey<FormState>();
  String _email, _password;
  bool _isObscured = true;
  Color _eyeButtonColor = Colors.grey;
  Future<String> _message = Future<String>.value('');

  String verificationId;
  String testSmsCode;

  Future<void> _testVerifyPhoneNumber(String Controller , String au) async {
    final PhoneVerificationCompleted verificationCompleted =
        (FirebaseUser user) {
      setState(() {
        _message =
            Future<String>.value('signInWithPhoneNumber auto succeeded: $user');
      });
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      setState(() {
        _message = Future<String>.value(
        'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
        });
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      Controller = testSmsCode;
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      this.verificationId = verificationId;
      Controller = testSmsCode;
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: au,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<String> _testSignInWithPhoneNumber(String Controller ,String smsCode) async {
    final FirebaseUser user = await _auth.signInWithPhoneNumber(
      verificationId: verificationId,
      smsCode: smsCode,
    );

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    Controller = '';
    return 'signInWithPhoneNumber succeeded: $user';
  }

  static Function(String s) Email(String invalidMessage) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  static Function(String s) PhoneNumberValidator(String invalidMessage) {
    String p = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  static Function(String s) Date(String invalidMessage) {
    String p = r'^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/([12]\d{3})$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  Padding buildTitle(String txt) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        txt,
        style: TextStyle(fontSize: 42.0),
      ),
    );
  }

  Padding buildTitleLine() {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0, left: 12.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          width: 38.0,
          height: 1.5,
          color: Colors.black,
        ),
      ),
    );
  }

  TextFormField buildEmailTextField() {
    return TextFormField(
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an email';
        }
      },
      decoration: InputDecoration(labelText: 'Email Address'),
    );
  }

  TextFormField PhoneTextField(TextEditingController controller) {
    return TextFormField(
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an email';
        }
      },
      decoration: InputDecoration(labelText: 'Mobile Number'),
      controller: controller,
      keyboardType: TextInputType.number,
    );
  }

  dob(AddUserModel au) {
    return CardSettingsField(
      requiredIndicator: Text('*', style: TextStyle(color: Colors.red)),
      label: 'Birth Date',

      content: new TextFormField(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(0.0),
          border: InputBorder.none,
          hintText: "DD/MM/YYYY",
        ),
        validator: (value) {
          if (value == "") {
            return "Date is required";
          } else {
            Date("Date is not valid");
          }
        },
        controller: au.addUserState.DOBController,
        keyboardType: TextInputType.datetime,
        inputFormatters: [
          WhitelistingTextInputFormatter.digitsOnly,
        ],

      ),
    );
  }

  TextFormField StateTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an State';
        }
      },
      decoration: InputDecoration(labelText: 'State'),
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.sentences,
    );
  }


  Padding buildPasswordText() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Align(
        alignment: Alignment.centerRight,
        child: Text(
          'Forgot Password?',
          style: TextStyle(fontSize: 12.0, color: Colors.grey),
        ),
      ),
    );
  }

  Align buildLoginButton(BuildContext context, AddUserModel au , String path) {
    return Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: () {
            au.VerifyeViewModel.onPressedCallback();
            Navigator.pushNamed(context, path);
          },
          color: Colors.grey[900],
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Verify',
            style: Theme.of(context).primaryTextTheme.button,
          ),
        ),
      ),
    );
  }

  Align buildVerifyButton(BuildContext context, AddUserModel au, String path) {
    return Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: () {
            if (au.addUserState.PhoneNumController.text != null) {
              au.phoneAuthButtonViewModel.onPressedCallback();
              Navigator.pushNamed(context , path);
            }
          },
          color: Colors.grey[900],
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Send Otp',
            style: Theme.of(context).primaryTextTheme.button,
          ),
        ),
      ),
    );
  }





  Align buildOrText() {
    return Align(
      alignment: Alignment.center,
      child: Text(
        'or login with',
        style: TextStyle(fontSize: 12.0, color: Colors.grey),
      ),
    );
  }

  Container buildSocialMediaButtons(IconData icon, Color iconColor) {
    return Container(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, "/SignUp");
        },
        borderRadius: BorderRadius.circular(30.0),
        child: Icon(icon, color: iconColor),
      ),
      height: 46.0,
      width: 46.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.grey, width: 0.5)),
    );
  }

  Align buildSignUpText(BuildContext context, String path) {
    return Align(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, path);
        },
        borderRadius: BorderRadius.circular(30.0),
        child: RichText(
          text: TextSpan(
              text: 'You Don\'t have an account?',
              style: TextStyle(fontSize: 12.0, color: Colors.grey),
              children: <TextSpan>[
                TextSpan(
                    text: ' SIGN UP',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                        color: Colors.black)),
              ]),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        child:  new StoreConnector<AppState, AddUserModel>(
        converter: (store) {
    return AddUserModel(
    addUserState: store.state.addUserState,
    loadingState: store.state.loadingState,
    onSave: () {
    store.dispatch(UserAdded());
    },
    );
    },
      builder: (BuildContext context, AddUserModel au) {
        return new Scaffold(
          body: Form(
            child: ListView(
              padding: const EdgeInsets.fromLTRB(
                  22.0, 0.0, 22.0, 22.0),
              children: <Widget>[
                SizedBox(height: kToolbarHeight),
                buildTitle('Enter The Code'),
                buildTitleLine(),
                SizedBox(
                  height: 70.0,
                ),
                CardSettings(
                  labelAlign: TextAlign.left,
                  labelPadding: 0.0,
                  contentAlign: TextAlign.left,
                  children: <Widget>[
                    SizedBox(
                      height: 40.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 16.0,right: 16.0),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'OTP',
                          prefixIcon: Icon(Icons.offline_pin)
                        ),
                        controller: au.addUserState.smsCodeController,
                        keyboardType: TextInputType.number,
                        validator: (value){
                          if (value.length != 6) {
                            return 'Please enter Correct OTP';
                          }
                        },
                      ),
                    ),
                    SizedBox(
                      height: 60.0,
                    ),
                    VerifyAuthButtonContainer(),
                    SizedBox(
                      height: 40.0,
                    ),
                  ],
                ),


                SizedBox(
                  height: 16.0,
                ),

                SizedBox(height: 70.0),

              ],
            ),
          ),
        );
      },
    ));
  }
}



