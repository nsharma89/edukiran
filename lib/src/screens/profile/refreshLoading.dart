part of app.widgets;

class refreshScreen extends StatefulWidget {
  refreshScreen({
    Key key,
  }) : super(key: key);

  @override
  _refreshScreenState createState() => _refreshScreenState();
}

class _refreshScreenState extends State<refreshScreen> {


  @override
  Widget build(BuildContext context) {
    return Stack(
        fit: StackFit.expand,
        children: [
          new TeddySpinner()
        ]);
  }
}

