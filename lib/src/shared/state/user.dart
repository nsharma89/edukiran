part of app.state;

class MeState {
  final User me;

  MeState({
    this.me,
  });

  factory MeState.initialFromUser(User me,) {
    return new MeState(
      me: me,

    );
  }

  MeState copyWith({
    User me,
  }) {
    var newState = new MeState(
      me: me ?? this.me,
    );
    return newState;
  }
}

final meStateReducer = combineReducers<AppState>([]);

//api call
class User {
  final TextEditingController uiid;

  final TextEditingController UserName ;

  final TextEditingController gender ;

  final TextEditingController email ;

  final TextEditingController phone ;

  final TextEditingController address ;

  final TextEditingController city;

  final TextEditingController DOB ;

  final TextEditingController state ;

  final TextEditingController age;

  final TextEditingController date_register ;

  final TextEditingController imageUri ;

  final TextEditingController Occupation;

  final TextEditingController EduCareId ;

  User({
    this.uiid,

    this.UserName
    ,
    this.gender,

    this.email,

    this.phone,

    this.address,

    this.city,

    this.DOB,

    this.state,

    this.age,

    this.date_register,

    this.imageUri,

    this.Occupation,

    this.EduCareId,
  }
  );

  factory User.initial(){
    return new User(
        uiid: new TextEditingController(),
        UserName: new TextEditingController(),
        gender: new TextEditingController(),
        email: new TextEditingController(),
        phone: new TextEditingController(),
        address: new TextEditingController(),
        city: new TextEditingController(),
        DOB: new TextEditingController(),
        state: new TextEditingController(),
        age: new TextEditingController(),
        date_register: new TextEditingController(),
        imageUri: new TextEditingController(),
        Occupation: new TextEditingController(),
        EduCareId: new TextEditingController(),
    );

  }

User copyWith({
   String uiid,

   String UserName ,

   String gender ,

   String email ,

   String phone ,

   String address ,

   String city,

   String DOB ,

   String state ,

   String age,

   String date_register ,

   String imageUri ,

   String Occupation,

   String EduCareId ,
}){
  return new User(
      uiid: uiid != null && uiid != "" ? uiid : this.uiid,
      UserName: UserName != null && UserName != "" ? UserName : this.UserName,
      gender: gender != null && gender != "" ? gender : this.gender,
      email: email != null && email != "" ? email : this.email,
      phone: phone != null && phone != "" ? phone : this.phone,
    address: address != null && address != "" ? address : this.address,
      city: city != null && city != "" ? city : this.city,
      DOB: DOB != null && DOB != "" ? DOB : this.DOB,
      state: state != null && state != "" ? state : this.state,
      age: age != null && age != "" ? age : this.age,
      date_register: date_register != null && date_register != "" ? date_register : this.date_register,
      imageUri: imageUri != null && imageUri != "" ? imageUri : this.imageUri,
      Occupation: Occupation != null && Occupation != "" ? Occupation : this.Occupation,
      EduCareId: EduCareId != null && EduCareId != "" ? EduCareId : this.EduCareId,
  );
}

  TextEditingController get username => UserName;


  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = UserName;

    return map;
  }










//  @override
//  String toString() {
//    return 'User[uiid=$uiid, UserName=$UserName, gender=$gender, email=$email, phone=$phone, address=$address,  city=$city, DOB=$DOB, state=$state, age=$age, imageUri=$imageUri, date_register=$date_register , Occupation=$Occupation, EduCareId=$EduCareId,]';
//  }
//
//  User.fromJson(Map<String, dynamic> json) {
//    if (json == null) return;
//    uiid = json['uiid'];
//
//    UserName = json['UserName'];
//
//    gender = json['gender'];
//    email = json['email'];
//
//    phone = json['phone'];
//
//    address = json['address'];
//
//    city = json['city'];
//
//    DOB = json['DOB'];
//
//    state = json['state'];
//
//    age = json['age'];
//
//    imageUri = json['imageUri'];
//
//    date_register = json['date_register'];
//
//    Occupation = json['Occupation'];
//
//    EduCareId = json['EduCareId'];
//  }
//
//  Map<String, dynamic> toJson() {
//    return {
//      'uiid': uiid,
//      'UserName': UserName,
//      'gender': gender,
//      'email': email,
//      'phone': phone,
//      'address': address,
//      'city': city,'DOB': DOB,
//      'state': state,
//      'age': age,
//      'date_register': date_register, 'imageUri': imageUri,'Occupation': Occupation,'EduCareId': EduCareId,
//    };
//  }
//
//  static List<User> listFromJson(List<dynamic> json) {
//    return json == null
//        ? new List<User>()
//        : json.map((value) => new User.fromJson(value)).toList();
//  }
//
//  static Map<String, User> mapFromJson(Map<String, Map<String, dynamic>> json) {
//    var map = new Map<String, User>();
//    if (json != null && json.length > 0) {
//      json.forEach((String key, Map<String, dynamic> value) =>
//          map[key] = new User.fromJson(value));
//    }
//    return map;
//  }
}

//api call   do  the part of ss.api
//class ApiException implements Exception {
//  int code = 0;
//  String message = null;
//  Exception innerException = null;
//  StackTrace stackTrace = null;
//
//  ApiException(this.code, this.message);
//
//  ApiException.withInner(
//      this.code, this.message, this.innerException, this.stackTrace);
//
//  String toString() {
//    if (message == null) return "ApiException";
//
//    if (innerException == null) {
//      return "ApiException $code: $message";
//    }
//
//    return "ApiException $code: $message (Inner exception: ${innerException})\n\n" +
//        stackTrace.toString();
//  }
//}
final addUserReducer = combineReducers<AppState>([
  TypedReducer<AppState, AddUserData1>(_addClassroomFormAgeFromUnitChangedAction),
]);


// AgeFrom action + reducer
class AddUserData1{
  final String uiid;

  final  String UserName ;

  final String gender ;

  final   String email ;

      final String phone ;

  final   String address ;

      final String city;

  final    String DOB ;

  final String state ;

  final   String age;

      final String date_register ;

  final     String imageUri ;

      final String Occupation;

  final   String EduCareId ;

      AddUserData1(

  this.uiid,

  this.UserName
  ,
  this.gender,

  this.email,

  this.phone,

  this.address,

  this.city,

  this.DOB,

  this.state,

  this.age,

  this.date_register,

  this.imageUri,

  this.Occupation,

  this.EduCareId,);
}
AppState _addClassroomFormAgeFromUnitChangedAction(AppState state, AddUserData1 action) {
  return state.copyWith(Auser: state.user.copyWith(
    uiid: action.uiid,
    UserName: action.UserName,
    gender: action.gender,
    email: action.email,
    phone: action.phone,
    address: action.address,
    city: action.city,
    DOB: action.DOB,
    state: action.state,
    age: action.age,
    date_register:action.date_register,
    imageUri: action.imageUri,
    Occupation: action.Occupation,
    EduCareId: action.EduCareId,
  ));
}///a.copyWith(ageFromUnit: action.value)dd htw reducers to all user