part of app.state;

class AppState {
  final LoadingState loadingState;
  final AddUserState addUserState;
  final User user;

  //superAdminStates
  final AddCourseVedio addCourseVedio;
  final AddYoutubeVedio addYoutubeVedio;
  final AddCategoryVedio addCategoryVedio;
  final AddAuthor addAuthor;


  // Utils
  final bool disableUserInputAndShowLoadingAnimation;
  final FirebaseUser firebaseUser;

  AppState({
    this.loadingState,
    this.firebaseUser,
    this.disableUserInputAndShowLoadingAnimation = false,
    this.addUserState,
    this.user,
    this.addYoutubeVedio,
    this.addCourseVedio,
    this.addCategoryVedio,
    this.addAuthor,
  });

  factory AppState.initial() {
    AppState s = new AppState(
      loadingState: LoadingState.initial(),
      addUserState: AddUserState.initial(),
      addYoutubeVedio: AddYoutubeVedio.initial(),
      addAuthor: AddAuthor.initial(),
      addCategoryVedio: AddCategoryVedio.initial(),
      addCourseVedio: AddCourseVedio.initial(),
      user: User.initial()
    );

    return s;
  }

  AppState copyWith({
    // all sub states
    LoadingState loadingState,
    AddUserState addUserState,
    User Auser,

    //superAdmin
     AddCourseVedio addCourseVedio,
     AddYoutubeVedio addYoutubeVedio,
     AddCategoryVedio addCategoryVedio,
     AddAuthor addAuthor,
    // others
    FirebaseUser firebaseUser,
    bool disableUserInputAndShowLoadingAnimation,
    bool disconnect,
  }) {
    FirebaseUser user;
    if (disconnect == true) {
      user = null;
    } else if (firebaseUser != null) {
      user = firebaseUser;
    } else {
      user = this.firebaseUser;
    }
    return new AppState(
      loadingState: loadingState ?? this.loadingState,
      addUserState: addUserState ?? this.addUserState,
      addYoutubeVedio: addYoutubeVedio ?? this.addYoutubeVedio,
      addAuthor: addAuthor ?? this.addAuthor,
      addCategoryVedio: addCategoryVedio ?? this.addCategoryVedio,
      addCourseVedio: addCourseVedio ?? this.addCourseVedio,
      user: Auser ?? this.user,
      // firebase user
      firebaseUser: user,

      // ui helper
      disableUserInputAndShowLoadingAnimation:
          disableUserInputAndShowLoadingAnimation ??
              this.disableUserInputAndShowLoadingAnimation,
    );
  }
}

final stateReducers = combineReducers<AppState>([
  formStateReducer,
  addUserReducer,
  addvedioreducer,
  addcoursereducer,
  addCategoryreducer,
  AddAuthorreducer,
]);
