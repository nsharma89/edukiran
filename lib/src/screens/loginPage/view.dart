part of app.widgets;

// At the top level:
enum SingingCharacter { Student, Teacher }

// In the State of a stateful widget:
SingingCharacter _character = SingingCharacter.Student;

class LoginPage extends StatefulWidget {
  LoginPage({
    Key key,
  }) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _email, _password;
  bool _isObscured = true;
  Color _eyeButtonColor = Colors.grey;
  Future<String> _message = Future<String>.value('');
  String selected = "+91";

  String verificationId;
   String testSmsCode;

  Future<void> _testVerifyPhoneNumber(String Controller , String au) async {
    final PhoneVerificationCompleted verificationCompleted =
        (FirebaseUser user) {
      setState(() {
        _message =
            Future<String>.value('signInWithPhoneNumber auto succeeded: $user');
      });
    };

    final PhoneVerificationFailed verificationFailed =
        (AuthException authException) {
      setState(() {
        _message = Future<String>.value(
        'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
        });
    };

    final PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      this.verificationId = verificationId;
      Controller = testSmsCode;
    };

    final PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      this.verificationId = verificationId;
      Controller = testSmsCode;
    };

    await _auth.verifyPhoneNumber(
        phoneNumber: au,
        timeout: const Duration(seconds: 5),
        verificationCompleted: verificationCompleted,
        verificationFailed: verificationFailed,
        codeSent: codeSent,
        codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
  }

  Future<String> _testSignInWithPhoneNumber(String Controller ,String smsCode) async {
    final FirebaseUser user = await _auth.signInWithPhoneNumber(
      verificationId: verificationId,
      smsCode: smsCode,
    );

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    Controller = '';
    return 'signInWithPhoneNumber succeeded: $user';
  }

  static Function(String s) Email(String invalidMessage) {
    String p =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  static Function(String s) PhoneNumberValidator(String invalidMessage) {
    String p = r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  static Function(String s) Date(String invalidMessage) {
    String p = r'^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/([12]\d{3})$';
    RegExp regExp = new RegExp(p);
    return (String val) => !regExp.hasMatch(val) ? invalidMessage : null;
  }

  Padding buildTitle(String txt) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        txt,
        style: TextStyle(fontSize: 42.0),
      ),
    );
  }

  Padding buildTitleLine() {
    return Padding(
      padding: const EdgeInsets.only(top: 4.0, left: 12.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Container(
          width: 38.0,
          height: 1.5,
          color: Colors.black,
        ),
      ),
    );
  }

  TextFormField buildEmailTextField() {
    return TextFormField(
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an email';
        }
      },
      decoration: InputDecoration(labelText: 'Email Address'),
    );
  }

  TextFormField PhoneTextField(TextEditingController controller) {
    return TextFormField(
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an email';
        }
      },
      decoration: InputDecoration(labelText: 'Mobile Number'),
      controller: controller,
      keyboardType: TextInputType.number,
    );
  }

  dob(AddUserModel au) {
    return CardSettingsField(
      requiredIndicator: Text('*', style: TextStyle(color: Colors.red)),
      label: 'Birth Date',

      content: new TextFormField(
        decoration: InputDecoration(
          contentPadding: EdgeInsets.all(0.0),
          border: InputBorder.none,
          hintText: "DD/MM/YYYY",
        ),
        validator: (value) {
          if (value == "") {
            return "Date is required";
          } else {
            Date("Date is not valid");
          }
        },
        controller: au.addUserState.DOBController,
        keyboardType: TextInputType.datetime,
        inputFormatters: [
          WhitelistingTextInputFormatter.digitsOnly,
        ],

      ),
    );
  }

  TextFormField StateTextField(TextEditingController controller) {
    return TextFormField(
      controller: controller,
      onSaved: (emailInput) => _email = emailInput,
      validator: (emailInput) {
        if (emailInput.isEmpty) {
          return 'Please enter an State';
        }
      },
      decoration: InputDecoration(labelText: 'State'),
      keyboardType: TextInputType.text,
      textCapitalization: TextCapitalization.sentences,
    );
  }


  Padding buildPasswordText() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Align(
        alignment: Alignment.centerRight,
        child: Text(
          'Forgot Password?',
          style: TextStyle(fontSize: 12.0, color: Colors.grey),
        ),
      ),
    );
  }

  Align buildLoginButton(BuildContext context, AddUserModel au , String path) {
    return Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: () {
            au.VerifyeViewModel.onPressedCallback();
            Navigator.pushNamed(context, path);
          },
          color: Colors.grey[900],
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Verify',
            style: Theme.of(context).primaryTextTheme.button,
          ),
        ),
      ),
    );
  }

  Align buildVerifyButton(BuildContext context, AddUserModel au, String path) {
    return Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: () {
            if (au.addUserState.PhoneNumController.text != null) {
              au.phoneAuthButtonViewModel.onPressedCallback();
              Navigator.pushNamed(context , path);
            }
          },
          color: Colors.grey[900],
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Send Otp',
            style: Theme.of(context).primaryTextTheme.button,
          ),
        ),
      ),
    );
  }





  Align buildOrText() {
    return Align(
      alignment: Alignment.center,
      child: Text(
        'or login with',
        style: TextStyle(fontSize: 12.0, color: Colors.grey),
      ),
    );
  }

  Container buildSocialMediaButtons(IconData icon, Color iconColor) {
    return Container(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, "/SignUp");
        },
        borderRadius: BorderRadius.circular(30.0),
        child: Icon(icon, color: iconColor),
      ),
      height: 46.0,
      width: 46.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          border: Border.all(color: Colors.grey, width: 0.5)),
    );
  }

  Align buildSignUpText(BuildContext context, String path) {
    return Align(
      child: InkWell(
        onTap: () {
          Navigator.pushNamed(context, path);
        },
        borderRadius: BorderRadius.circular(30.0),
        child: RichText(
          text: TextSpan(
              text: 'You Don\'t have an account?',
              style: TextStyle(fontSize: 12.0, color: Colors.grey),
              children: <TextSpan>[
                TextSpan(
                    text: ' SIGN UP',
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 14.0,
                        color: Colors.black)),
              ]),
        ),
      ),
    );
  }


  @override
  Widget build(BuildContext context) {
    return Container(
        child: new WidgetsApp(
      title: "Edu Kiran",
      supportedLocales: [
        const Locale('en', 'US'), // English
      ],
      color: Colors.pink,
      initialRoute: "/",
      onGenerateRoute: (RouteSettings settings) {
        debugPrint(settings.name);
        return new TeddySlideTransitionRoute(
            settings: settings,
            builder: (context) {
              switch (settings.name) {
//                case "/a":
//                  print("Camera Called");
//                  return new StoreConnector<AppState, AddUserModel>(
//                    converter: (store) {
//                      return AddUserModel(
//                        addUserState: store.state.addUserState,
//                        loadingState: store.state.loadingState,
//                        onOccupationStudent: (s) {
//                          store.dispatch(OccupationDropdownChange('Student'));
//                        },
//                        onOccupationTEacher: (s) {
//                          store.dispatch(OccupationDropdownChange('Teacher'));
//                        },
//                        onSave: () {
//                          store.dispatch(UserAdded());
//                        },
//                      );
//                    },
//                    builder: (BuildContext context, AddUserModel au) {
//                      return new Scaffold(
//                        body: Form(
//                          child: ListView(
//                            padding: const EdgeInsets.fromLTRB(
//                                22.0, 0.0, 22.0, 22.0),
//                            children: <Widget>[
//                              SizedBox(height: kToolbarHeight),
//                              buildTitle('Sign Up'),
//                              buildTitleLine(),
//                              SizedBox(
//                                height: 20.0,
//                              ),
//                              CardSettings(
//                                labelAlign: TextAlign.left,
//                                labelPadding: 0.0,
//                                contentAlign: TextAlign.left,
//                                children: <Widget>[
//
//                                  SizedBox(
//                                    height: 10.0,
//                                  ),
//                                  CardSettingsText(
//                                    controller:au.addUserState.UserNameController ,
//                                    label: "Username",
//                                    hintText: "Ram Sharma",
//
//                                  ),
//                                  CardSettingsEmail(
//                                    label: "Email",
//                                    labelAlign: TextAlign.left,
//                                    controller: au.addUserState.emailController,
//                                    validator: Email("Invalid Email "),
//                                  ),
//                                  SizedBox(
//                                    height: 10.0,
//                                  ),
//                                  CardSettingsField(
//                                    label: "Occupation",
//
//                                    labelAlign: TextAlign.left,
//                                    content: Column(
//                                      children: <Widget>[
//                                        RadioListTile<SingingCharacter>(
//                                          title: const Text('Student'),
//                                          value: SingingCharacter.Student,
//                                          groupValue: _character,
//                                          onChanged: (SingingCharacter value) { setState(() { _character = value; });
//                                          au.onOccupationStudent(value);
//                                          },
//                                        ),
//                                        RadioListTile<SingingCharacter>(
//                                          title: const Text('Teacher'),
//                                          value: SingingCharacter.Teacher,
//                                          groupValue: _character,
//                                          onChanged: (SingingCharacter value) { setState(() { _character = value;
//                                          au.onOccupationTEacher(value);
//                                          }); },
//                                        ),
//                                      ],
//                                    )
//                                  ),
//                                  SizedBox(
//                                    height: 30.0,
//                                  ),
//                                  dob(au),
//                                  CardSettingsText(
//                                    controller: au.addUserState.GenderController,
//                                    label: "Gender",
//                                    hintText: "Male",
//
//                                  ),
//                                  CardSettingsText(
//                                    controller: au.addUserState.AddressController,
//                                    label: "Address",
//                                    hintText: "Flat no - 0 1",
//
//                                  ),
//                                  CardSettingsText(
//                                    controller:au.addUserState.StateController ,
//                                    label: "State",
//                                    hintText: "New Delhi",
//
//                                  ),
//                                  CardSettingsText(
//                                    controller:au.addUserState.CityController ,
//                                    label: "City",
//                                    hintText: "Jaipur",
//                                  ),
//
//                                  SizedBox(
//                                    height: 30.0,
//                                  ),
//
//
//
//
//                                ],
//                              ),
//                              SizedBox(
//                                height: 30.0,
//                              ),
//
//
//
//                              SizedBox(height: 70.0),
//
//                            ],
//                          ),
//                        ),
//                      );
//                      ;
//                    },
//                  );
//                  break;
//
//                case "/otp":
//                  print("OTP Screen Called");
//                  return new StoreConnector<AppState, AddUserModel>(
//                    converter: (store) {
//                      return AddUserModel(
//                        addUserState: store.state.addUserState,
//                        loadingState: store.state.loadingState,
//                        onSave: () {
//                          store.dispatch(UserAdded());
//                        },
//                      );
//                    },
//                    builder: (BuildContext context, AddUserModel au) {
//                      return new Scaffold(
//                        body: Form(
//                          child: ListView(
//                            padding: const EdgeInsets.fromLTRB(
//                                22.0, 0.0, 22.0, 22.0),
//                            children: <Widget>[
//                              SizedBox(height: kToolbarHeight),
//                              buildTitle('Enter The Code'),
//                              buildTitleLine(),
//                              SizedBox(
//                                height: 70.0,
//                              ),
//                              CardSettings(
//                                labelAlign: TextAlign.left,
//                                labelPadding: 0.0,
//                                contentAlign: TextAlign.left,
//                                children: <Widget>[
//                                  SizedBox(
//                                    height: 40.0,
//                                  ),
//                                  Padding(
//                                    padding: const EdgeInsets.only(left: 16.0,right: 16.0),
//                                    child: TextFormField(
//
//                                      decoration: InputDecoration(
//                                        labelText: 'OTP',
//                                      ),
//                                      controller: au.addUserState.smsCodeController,
//                                      keyboardType: TextInputType.number,
//
//                                    ),
//                                  ),
//                                  SizedBox(
//                                    height: 60.0,
//                                  ),
//                                  PhoneAuthButtonContainer(),
//                                  SizedBox(
//                                    height: 40.0,
//                                  ),
//                                ],
//                              ),
//
//
//                              SizedBox(
//                                height: 16.0,
//                              ),
//
//                              SizedBox(height: 70.0),
//
//                            ],
//                          ),
//                        ),
//                      );
//                    },
//                  );
//                  break;
                default:
                  return new StoreConnector<AppState, AddUserModel>(
                    converter: (store) {
                      return AddUserModel(
                        addUserState: store.state.addUserState,
                        loadingState: store.state.loadingState,
                          onMobilecode: (s){
                            store.dispatch(CodeDropdownChange(s));
                           }
                      );
                    },
                    builder: (BuildContext context, AddUserModel au) {
                      return Scaffold(
                        body: Form(
                          child: ListView(
                            padding: const EdgeInsets.fromLTRB(
                                22.0, 0.0, 22.0, 22.0),
                            children: <Widget>[
                              SizedBox(height: kToolbarHeight),
                              buildTitle('Welcome to EduTuts'),
                              buildTitleLine(),
                              SizedBox(
                                height: 70.0,
                              ),
                              CardSettings(
                                labelAlign: TextAlign.left,
                                labelPadding: 0.0,
                                contentAlign: TextAlign.left,
                                children: <Widget>[
                                  SizedBox(
                                    height: 40.0,
                                  ),
                              Padding(
                                padding: const EdgeInsets.only(left: 16.0,right: 16.0),
                                child: Column(

                                  children: <Widget>[
//                                    CardSettingsListPicker(
//                                      label: "Mobile Code",
//                                      options:au.getCode2() ,
//                                      initialValue: "+91",
//
////                                      items: au.getCode(),
////                                      value: selected,
////                                      hint: Text("+91"),
//                                      onChanged:  (v){
//                                      selected = v;
//                                        au.onMobilecode(v); },
////                                      elevation: 1,
////                                        isDense: true,
//
//                                    ),

                                    Form(

                                      child: TextFormField(
                                        onSaved: (passwordInput) => _password = passwordInput,
                                        validator: (s) {

                                          PhoneNumberValidator("Enter the correct Mobile Number");
                                          },
                                        decoration: InputDecoration(
                                          labelText: 'Your Phone Number',
                                          prefix: Text("+91"),
                                          fillColor: Colors.teal,
                                          prefixIcon: Icon(Icons.phone),
                                        ),
                                        controller: au.addUserState.PhoneNumController,
                                        keyboardType: TextInputType.number,
                                        key: au.addUserState.verify,


                                      ),
                                      autovalidate: _autoValidate ,
                                      key: _formKey,
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                    height: 60.0,
                                  ),

                                  PhoneAuthButtonContainer(),
                                  SizedBox(
                                    height: 40.0,
                                  ),
                                ],
                              ),


                              SizedBox(
                                height: 16.0,
                              ),

                              SizedBox(height: 70.0),

                            ],
                          ),
                        ),
                      );
                    },
                  );
                  break;
              }
            });
      },
    ));
  }
}

class AddUserModel {
  final AddUserState addUserState;
  final LoadingState loadingState;
  final AddYoutubeVedio  addYoutubeVedio;
  final User user;
  final _PhoneAuthButtonViewModel phoneAuthButtonViewModel;
  final void Function(dynamic s) onOccupationStudent;
  final void Function(dynamic s) onOccupationTEacher;
  final void Function(dynamic s) onGenderChange;
  final _VerifyeViewModel VerifyeViewModel;
  final AppState appState;

  final void Function(dynamic s) onMobilecode;

  final void Function(dynamic s) addSubject;

  final void Function() onSave;
  final void Function(BuildContext context) onSave2;
  final void Function() anything;
  final void Function(dynamic s) navigate;

  AddUserModel({
    this.addUserState,
    this.addYoutubeVedio,
    this.onOccupationStudent,
    this.onOccupationTEacher,
    this.loadingState,
    this.onSave,
    this.phoneAuthButtonViewModel,
    this.VerifyeViewModel,
    this.appState,
    this.user,
    this.onMobilecode,
    this.addSubject,
    this.onGenderChange,
    this.navigate,
    this.anything,
    this.onSave2
  });

  List<String> getOccupation() {
    return ['Teacher', 'Student'];
  }

  List<DropdownMenuItem> getCode() {
    return <String>['+1', '+91', '+242', '+246'].map((String value) {
      return new DropdownMenuItem<String>(
        value: value,
        child: new Text(value),


      );
    }).toList();
  }

  List<String> getCode2() {
    return ['+1', '+91', '+242', '+246'];
  }

}


class VerifyAuthButtonContainer extends StatelessWidget {
  VerifyAuthButtonContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Connect to the store:
    return new StoreConnector<AppState, _VerifyeViewModel>(
      converter: _VerifyeViewModel.fromStore,
      builder: (BuildContext context, _VerifyeViewModel vm) {
        // We haven't made this yet.
        return new VerifyAuthButton(
          onPressedCallback: vm.onPressedCallback,
        );
      },
    );
  }
}

class _VerifyeViewModel {

  final Function onPressedCallback;

  _VerifyeViewModel({this.onPressedCallback});

  static _VerifyeViewModel fromStore(Store<AppState> store) {
    return new _VerifyeViewModel(
        onPressedCallback: () {

            if (store.state.firebaseUser != null) {
              store.dispatch(new fetchUserData());
            } else {


              store.dispatch(new PhoneVerificationLogInAction());
                    //
            }

        });

  }
}



class VerifyAuthButton extends StatelessWidget {

  final Function onPressedCallback;

  // Passed in from Container
  VerifyAuthButton({
    this.onPressedCallback,
  });

  @override
  Widget build(BuildContext context) {
    return  Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: onPressedCallback,
          color: Colors.teal,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'Verify',

          ),
        ),
      ),
    );
  }
}

class PhoneAuthButtonContainer extends StatelessWidget {
  PhoneAuthButtonContainer({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Connect to the store:
    return new StoreConnector<AppState, _PhoneAuthButtonViewModel>(
      converter: _PhoneAuthButtonViewModel.fromStore,
      builder: (BuildContext context, _PhoneAuthButtonViewModel vm) {
        // We haven't made this yet.
        return new PhoneAuthButton(
          onPressedCallback: vm.onPressedCallback,
        );
      },
    );
  }
}

class _PhoneAuthButtonViewModel {

  final Function onPressedCallback;


  _PhoneAuthButtonViewModel({this.onPressedCallback ,});

  static _PhoneAuthButtonViewModel fromStore(Store<AppState> store) {
    return new _PhoneAuthButtonViewModel(
        onPressedCallback: () {

         if (store.state.firebaseUser != null) {
           store.dispatch(new fetchUserData());
         } else {
           store.dispatch(new PhoneLogInAction());
           store.dispatch(new NavigateOtpAction());
         }
       });
  }
}



class PhoneAuthButton extends StatefulWidget {

  final Function onPressedCallback;

  // Passed in from Container
  PhoneAuthButton({
    this.onPressedCallback,
  });

  @override
  PhoneAuthButtonState createState() {
    return new PhoneAuthButtonState();
  }
}

class PhoneAuthButtonState extends State<PhoneAuthButton> {
  @override
  Widget build(BuildContext context) {
    return  Align(
      child: SizedBox(
        height: 50.0,
        width: 270.0,
        child: FlatButton(
          onPressed: widget.onPressedCallback,
          color: Colors.teal,
          shape:
          RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          child: Text(
            'GET OTP',
          ),
        ),
      ),
    );
  }
}